// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'dart:io' as io;
import 'package:test/test.dart';

void main() {

    final testFolder = io.Directory(p.join(p.current, p.current, '.dart_tool', 'test', 'file_system'));
    final fileA = io.File(p.join(testFolder.path, 'a'));
    final folderB = io.Directory(p.join(testFolder.path, 'b'));
    final fileB = io.File(p.join(folderB.path, 'b'));
    final emptyLinkC = io.Link(p.join(testFolder.path, 'c'));
    final fileLinkD = io.Link(p.join(testFolder.path, 'd'));
    final folderLinkE = io.Link(p.join(testFolder.path, 'e'));
    final dummy = 'dummy';

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    // recursive: true だと link で error が発生するので 分ける必要がある.
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        folderB.createSync();
        fileB.createSync();
        emptyLinkC.createSync('dummy');
        fileLinkD.createSync(fileA.path);
        folderLinkE.createSync(p.join(p.current, folderB.path));

    });

    group('fromPath', () {
        // Pipe と UnixDomainSocket は windows では test できない.
        test('expected: return File then exists file', () async {
            final value = fileA.path;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = File(p.join(p.current, '.dart_tool', 'test', 'file_system'), 'a', value);
            expect(actual, expected);
        });
        test('expected: return Folder then exists folder', () async {
            final value = folderB.path;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = Folder(p.join(p.current, '.dart_tool', 'test', 'file_system'), 'b', value);
            expect(actual, expected);
        });
        test('expected: return EmptyLink then exists empty link', () async {
            final value = emptyLinkC.path;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = EmptyLink(p.join(p.current, '.dart_tool', 'test', 'file_system'), 'c', value);
            expect(actual, expected);
        });
        test('expected: return FileLink then exists file link', () async {
            final value = fileLinkD.path;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = FileLink(p.join(p.current, '.dart_tool', 'test', 'file_system'), 'd', value);
            expect(actual, expected);
        });
        test('expected: return FolderLink then exists folder link', () async {
            final value = folderLinkE.path;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLink(p.join(p.current, '.dart_tool', 'test', 'file_system'), 'e', value);
            expect(actual, expected);
        });
        test('expected: return NotFound then not exists', () async {
            final value = dummy;
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = NotFound('.', dummy, value);
            expect(actual, expected);
        });
        test('expected: return Folder then root', () async {
            final value = 'C:\\';
            final base = FileSystem.fromPath(value);
            final result = base;
            final actual = result.wrapped;
            final expected = Folder('C:\\', '', value);
            expect(actual, expected);
        });
    });

    group('fromLocationAndName', () {
        test('expected: return FileSystem then ', () {
            final location = testFolder.path;
            final name = 'a';
            final base = FileSystem.fromLocationAndName(location, name);
            final result = base;
            final actual = result.wrapped;
            final expected = File(location, name, p.join(location, name));
            expect(actual, expected);
        });
    });

    group('change', () {
        test('expected: return FileSystem then change name', () {
            final name = 'b';
            final base = FileSystem.fromPath(fileA.path).asExpected;
            final result = base.change(name: name);
            final actual = result;
            final expected = Folder(testFolder.path, name, p.join(testFolder.path, name));
            expect(actual, expected);
        });
        // test('expected: return FileSystem then change location', () {
        //     final location = testFolder.path;
        //     final base = FileSystem.fromPath(folderB.path).asExpected;
        //     final result = base.change(location: location);
        //     final actual = result;
        //     final expected = Folder(testFolder.path, name, p.join(testFolder.path, name));
        //     expect(actual, expected);
        // });
    });

    group('getNewPath', () {
        test('expected: return String then location', () {
            final location = 'a';
            final base = FileSystem.fromPath(fileA.path).asExpected;
            final result = base.getNewPath(location: location);
            final actual = result;
            final expected = p.join('a', 'a');
            expect(actual, expected);
        });
        test('expected: return String then name', () {
            final name = 'b';
            final base = FileSystem.fromPath(fileA.path).asExpected;
            final result = base.getNewPath(name: name);
            final actual = result;
            final expected = p.join(testFolder.path, 'b');
            expect(actual, expected);
        });
        test('expected: return String then location and name', () {
            final location = 'a';
            final name = 'b';
            final base = FileSystem.fromPath(fileA.path).asExpected;
            final result = base.getNewPath(location: location, name: name);
            final actual = result;
            final expected = p.join('a', 'b');
            expect(actual, expected);
        });
    });

}
