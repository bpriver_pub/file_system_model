// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:io' as io;
import 'dart:typed_data';

// import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    
    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['.dart_tool', 'test', 'atom', 'file_system_model', 'entry_point']);

    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_system_model', 'entry_point'));
    
    final nameA = 'a';
    final contentA = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentD = fileA.path;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    final nameDummy = 'dummy';
    final fileDummy = io.File(p.join(testFolder.path, nameDummy));

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentA);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentD);
        folderLinkE.createSync(contentE);

    });

    group('fromPath', () {
        test('expected: return FileModel then 指定された path に存在するのが file.', () async {
            final path = fileA.path;
            final base = await FileSystemModel.fromPath(path);
            final result = base;
            final actual = (result.wrapped as FileModel).decode().wrapped;
            final expected = FileModel(nameA, contentA);
            expect(actual, expected);
        });
        test('expected: return FolderModel then 指定された path に存在するのが folder.', () async {
            final path = folderB.path;
            final base = await FileSystemModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderModel(nameB, FileSystemModelRoster<FileSystemModel>.empty());
            expect(actual, expected);
        });
        test('expected: return LinkModel then 指定された path に存在するのが link.', () async {
            final path = fileLinkD.path;
            final base = await FileSystemModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = FileLinkModel(nameD, fileA.path);
            expect(actual, expected);
        });
        test('exception: return FileSystemException then 指定された path に存在するのが file でも folder でも link でもない.', () async {
            final path = p.join(testFolder.path, 'dummy');
            final base = await FileSystemModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'path': path,
            //         'file system entity type': NotFound,
            //     };
            //     expect(actual, expected);
            // }
        });
    });

    group('rename', () {
        test('expected: return Complete then file path', () async {
            final temp = testFolder.createTempSync('rename_a');
            final fileA = io.File(p.join(temp.path, 'a'));
            fileA.createSync();
            final path = fileA.path;
            final newName = 'new_name';
            final newFile = io.File(p.join(temp.path, newName));
            expect(fileA.existsSync(), true);
            expect(newFile.existsSync(), false);

            final base = await FileSystemModel.rename(path, newName);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            // debug.writeLoggerResult(result);
            expect(actual, expected);

            expect(fileA.existsSync(), false);
            expect(newFile.existsSync(), true);
        });
        test('expected: return Complete then folder path', () async {
            final path = folderB.path;
            final newName = 'new_name';
            final newFolder= io.Directory(p.join(testFolder.path, newName));
            
            expect(folderB.existsSync(), true);
            expect(newFolder.existsSync(), false);

            final base = await FileSystemModel.rename(path, newName);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(folderB.existsSync(), false);
            expect(newFolder.existsSync(), true);
        });
        test('expected: return Complete then folder link', () async {
            final path = fileLinkD.path;
            final newName = 'new_name';
            final newLink= io.Link(p.join(testFolder.path, newName));
            
            expect(fileLinkD.existsSync(), true);
            expect(newLink.existsSync(), false);

            final base = await FileSystemModel.rename(path, newName);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(fileLinkD.existsSync(), false);
            expect(newLink.existsSync(), true);
        });
        test('exception: return Complete then already exists name', () async {
            final temp = testFolder.createTempSync('e');
            final fileA = io.File(p.join(temp.path, 'a'));
            final fileB = io.File(p.join(temp.path, 'b'));
            fileA.createSync();
            fileB.createSync();

            final path = fileA.path;
            final newName = 'b';
            expect(fileA.existsSync(), true);
            expect(fileB.existsSync(), true);

            final base = await FileSystemModel.rename(path, newName);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
        });
    });

    group('copy', () {
        test('expected: return Complete then copy file ', () async {
            final temp = testFolder.createTempSync('copy_file');
            temp.createSync();
            final file = io.File(p.join(temp.path, nameA));
            expect(file.existsSync(), false);

            final copyPath = fileA.path;
            final pasteLocation = temp.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(file.existsSync(), true);
        });
        test('expected: return Complete then copy file and exists file', () async {
            final temp = testFolder.createTempSync('copy_copy_file_exists');
            final file = io.File(p.join(temp.path, 'a'));
            file.createSync();
            file.writeAsStringSync('alreday exists file');
            expect(file.existsSync(), true);
            expect(file.readAsStringSync() == 'alreday exists file', true);

            final copyPath = fileA.path;
            final pasteLocation = temp.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            // debug.writeLoggerResult(result);
            expect(actual, expected);

            expect(file.existsSync(), true);
            expect(file.readAsStringSync() == contentA, true);
        });
        test('expected: return Complete then copy folder ', () async {
            final folder = io.Directory(p.join(testFolder.path, 'copy_folder'));
            folder.createSync();
            final inFile = io.File(p.join(folderB.path, 'in_file'));
            inFile.createSync(); // folder の中身も copy されているかの確認.
            final pasted = io.Directory(p.join(folder.path, nameB));
            final pastedInFile = io.File(p.join(pasted.path, 'in_file'));
            expect(pasted.existsSync(), false);
            expect(pastedInFile.existsSync(), false);

            final copyPath = folderB.path;
            final pasteLocation = folder.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(pasted.existsSync(), true);
            expect(pastedInFile.existsSync(), true);
        });
        test('expected: return Complete then copy link ', () async {
            final folder = io.Directory(p.join(testFolder.path, 'copy_link'));
            folder.createSync();
            final pasted = io.Link(p.join(folder.path, nameD));
            expect(pasted.existsSync(), false);

            final copyPath = fileLinkD.path;
            final pasteLocation = folder.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(pasted.existsSync(), true);
        });
        test('expected: return Complete then copy link and exists file', () async {
            final temp = testFolder.createTempSync('copy_copy_link_exists_file');
            final link = io.Link(p.join(temp.path, nameD));
            // fileLinkD は fileA を link
            link.createSync(folderB.path);
            expect(link.existsSync(), true);
            // final a = link.targetSync();
            // print(a);
            expect(link.targetSync(), folderB.path);

            final copyPath = fileLinkD.path;
            final pasteLocation = temp.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(link.existsSync(), true);
            expect(link.targetSync(), fileA.path);

        });
        test('exception: return FileSystemException then copy not file, folder, link ', () async {
            final folder = io.Directory(p.join(testFolder.path, 'not_copy'));
            folder.createSync();
            final copyPath = fileDummy.path;
            final pasteLocation = folder.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'file system type': NotFound,
            //     };
            //     expect(actual, expected);
            // }
        });
        test('exception: return FileSystemException then past 先に folder', () async {
            final folder = io.Directory(p.join(testFolder.path, 'exists_folder'));
            folder.createSync();
            final existsFolder = io.Directory(p.join(folder.path, nameDummy));
            existsFolder.createSync();

            final copyPath = fileDummy.path;
            final pasteLocation = folder.path;
            final base = await FileSystemModel.copy(copyPath, pasteLocation);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'file system type': Folder,
            //     };
            //     expect(actual, expected);
            // }
        });
    });

    group('move', () {
        test('expected: return Complete then ', () async {
            final temp = testFolder.createTempSync('move_a');

            final path = fileA.path;
            final location = temp.path;

            final file = io.File(p.join(temp.path, nameA));
            expect(file.existsSync(), false);
            expect(fileA.existsSync(), true);

            final base = await FileSystemModel.move(path, location);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(file.existsSync(), true);
            expect(fileA.existsSync(), false);
        });
        test('exception: return FileSystemException then path is not file or folder, link.', () async {
            final temp = testFolder.createTempSync('move_c');

            final path = fileDummy.path;
            final location = temp.path;
            final base = await FileSystemModel.move(path, location);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
        });
        test('exception: return FileSystemException then 移動先にfile system が存在.', () async {
            final temp = testFolder.createTempSync('move_b');

            final file = io.File(p.join(temp.path, 'b'));
            file.createSync();

            final path = file.path;
            final location = testFolder.path;
            final base = await FileSystemModel.move(path, location);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
        });
    });

    group('fromJson', () {
        final name = 'a';
        test('expected: return FileModel then ', () {
            final json = {
                'FileModel': {
                    'name': name,
                    'content': {
                        'BinaryContent': {
                            'value': [0, 1, 2],
                        },
                    },
                }
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileModel.uint8List(name, Uint8List.fromList([0, 1, 2]));
            expect(actual, expected);
        });
        test('expected: return FolderModel then ', () {
            final json = {
                'FolderModel': {
                    'name': name,
                    'content': {
                        'FileSystemModelRoster': {
                            'values': <Object>[],
                        },
                    },
                }
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderModel(name, FileSystemModelRoster.empty());
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('expected: return EmptyLinkModel then ', () {
            final content = 'x';
            final json = {
                'EmptyLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = EmptyLinkModel(name, content);
            expect(actual, expected);
        });
        test('expected: return FileLinkModel then ', () {
            final content = 'x';
            final json = {
                'FileLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileLinkModel(name, content);
            expect(actual, expected);
        });
        test('expected: return FolderLinkModel then ', () {
            final content = 'x';
            final json = {
                'FolderLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLinkModel(name, content);
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then ', () {
            final json = {
                'DummyContent': 'dummy'
            };
            final base = FileSystemModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid key set': {'FileModel', 'FolderModel', 'EmptyLinkModel', 'FileLinkModel', 'FolderLinkModel'},
                };
                expect(actual, expected);
            }
        });
    });

    group('primaryKey', () {
        test('expected: return String', () {
            final base = FileModel(nameA, contentA);
            final result = base.primaryKey;
            final actual = result;
            final expected = 'a';
            expect(actual, expected);
        });
    });

    group('delete', () {
        // final location = testFolder.path;
        test('expected: return Complete then path is file', () async {
            expect(fileA.existsSync(), true);
            final base = await FileSystemModel.delete(fileA.path);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileA.existsSync(), false);
        });
        test('expected: return Complete then path is folder', () async {
            expect(folderB.existsSync(), true);
            final base = await FileSystemModel.delete(folderB.path);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderB.existsSync(), false);
        });
        test('expected: return Complete then path is link', () async {
            expect(fileLinkD.existsSync(), true);
            final base = await FileSystemModel.delete(fileLinkD.path);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileLinkD.existsSync(), false);
        });
        test('exception: return FileSystemException then path is notFound', () async {
            final fileDummy = io.File(p.join(testFolder.path, 'dummy'));
            expect(fileDummy.existsSync(), false);
            final base = await FileSystemModel.delete(fileDummy.path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            expect(fileDummy.existsSync(), false);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'path': fileDummy.path,
            //         'file system entity type': NotFound,
            //     };
            //     expect(actual, expected);
            // }
        });
        // Pipe, UnixDomainSocket の case は windows で test できない.
    });

}
