// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

// import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:image/image.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['atom', 'file_system_model']);
    
    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_model'));
    final location = testFolder.path;

    final nameA = 'a';
    final contentX = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentA = nameA;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentX);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentA);
        folderLinkE.createSync(contentE);

    });

    group('fromPath', () {
        test('expected: return FileModel.', () async {
            final path = fileA.path;
            final base = await FileModel.fromPath(path);
            final result = base;
            final actual = (result.wrapped as FileModel).decode().wrapped;
            final expected = FileModel(nameA, contentX);
            expect(actual, expected);
        });
        test('expected: return FileModel then content is binary.', () async {
            final image = Image(width: 256, height: 256);
            for (final pixel in image) {
                // Set the pixels red value to its x position value, creating a gradient.
                // Set the pixels green value to its y position value.
                pixel.r = pixel.x;
                pixel.g = pixel.y;
            }
            // Encode the resulting image to the PNG image format.
            final png = encodePng(image);
            // Write the PNG formatted data to a file.
            final pngName = 'image.png';
            final pngFile = io.File(p.join(location, pngName));
            pngFile.createSync();
            await pngFile.writeAsBytes(png);

            final path = pngFile.path;
            final base = await FileModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<FileModel>();
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('exception: return FileSystemException.', () async {
            final path = folderB.path;
            final base = await FileModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'path': path,
            //     };
            //     expect(actual, expected);
            // }
            expect(actual, expected);
        });
    });

    group('fromLocationAndName', () {
        test('expected: return FileModel then ', () async {
            final base = await FileModel.fromLocationAndName(location, nameA);
            final result = base;
            final actual = (result.wrapped as FileModel).decode().wrapped;
            final expected = FileModel(nameA, contentX);
            expect(actual, expected);
        });
        test('exception: return FileSystemException.', () async {
            final base = await FileModel.fromLocationAndName(location, nameB);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
        });
    });

    group('fromJson', () {
        final name = 'a';
        final list = <int>[0, 1, 2];
        test('expected: return FileModel then binary content', () {
            final file = FileModel.uint8List(name, Uint8List.fromList(list));
            final json = file.toJson();
            final base = FileModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = file;
            expect(actual, expected);
        });
        test('expected: return FileModel then string content', () {
            final file = FileModel(name, 'a');
            final json = file.toJson();
            final base = FileModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = file;
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then class key is invalid', () {
            final json = {
                'DummyClass': {
                    'name': name,
                    'content': {
                        'BinaryContent': {
                            'value': list,
                        }
                    }
                }
            };
            final base = FileModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid class key': 'FileModel',
                };
                expect(actual, expected);
            }
        });
    });

    group('changeExhaustive()', () {
        test('expected: return FileModel then name editor and file content editor is not null then return both edited', () {
            final base = FileModel(nameA, contentX);
            final result = base.changeExhaustive(
                (old) {
                    switch (old) {
                    case FileModel():
                        final content = old.content;
                        switch (content) {
                        case StringContent(): return FileModel(old.name + old.name, content.value + content.value);
                        case BinaryContent(): return FileModel.uint8List(old.name + old.name, Uint8List.fromList(content.value + content.value));
                        }
                        
                    case _:
                        return old;
                    }
                },
            );
            final actual = result.wrapped;
            final expected = FileModel(
                nameA + nameA,
                contentX + contentX,
            );
            expect(actual, expected);
        });
    });

    group('createAsComplement', () {
        test('expected: return Complete then 既存の file が存在しない場合新規に作成する.', () async {
            final name = 'x';
            final content = 'xxx';
            final file = io.File(p.join(location, name));
            expect(file.existsSync(), false);
            final base = FileModel(name, content);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(file.existsSync(), true);
            expect(file.readAsStringSync(), content);
        });
        test('expected: return Complete then 既存の file が存在する場合何もしない.', () async {
            final fileANewContent = 'xxx';
            final base = FileModel(nameA, fileANewContent);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileA.existsSync(), true);
            expect(fileA.readAsStringSync(), contentX);
        });
    });

    group('createAsOverwrite', () {
        test('expected: return Complete then 既存の file が存在しない場合新規に作成する.', () async {
            final name = 'x';
            final content = 'xxx';
            final file = io.File(p.join(location, name));
            expect(file.existsSync(), false);
            final base = FileModel(name, content);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(file.existsSync(), true);
            expect(file.readAsStringSync(), content);
        });
        test('expected: return Complete then 既存の file が存在する場合上書きする.', () async {
            final fileANewContent = 'xxx';
            final base = FileModel(nameA, fileANewContent);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileA.existsSync(), true);
            expect(fileA.readAsStringSync(), fileANewContent);
        });
        test('expected: return Complete then 既存の file 以外の file system が存在する場合 それを削除し 新規作成する.', () async {
            final fileB = io.File(folderB.path);
            expect(fileB.existsSync(), false);
            final content = 'xxx';
            final base = FileModel(nameB, content);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderB.existsSync(), false);
            expect(fileB.existsSync(), true);
            expect(fileB.readAsStringSync(), content);
        });
    });

    group('decode', () {
        test('expected: return FileModel then content is string', () {
            final base = FileModel(nameA, contentA);
            final result = base.decode();
            final actual = result.wrapped;
            final expected = base;
            expect(actual, expected);
        });
        test('expected: return FileModel then content is binary', () {
            final content = utf8.encode(contentA);
            final base = FileModel.uint8List(nameA, content);
            final result = base.decode();
            final actual = result.wrapped;
            final expected = FileModel(nameA, contentA);
            expect(actual, expected);
        });
        test('exception: return FormatException then content contains invalid byte', () {
            final content = Uint8List.fromList([10000000]);
            final base = FileModel.uint8List(nameA, content);
            final result = base.decode(utf8);
            final actual = result.wrapped;
            final expected = isA<FormatException>();
            expect(actual, expected);
        });
    });

}
