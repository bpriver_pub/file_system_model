// Copyright (C) 2025, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

// ignore_for_file: unused_import

import 'dart:io' as io;
import 'dart:typed_data';

// import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['atom', 'file_system_model', 'file', 'file_content']);
    
    // final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_system_model', 'file', 'file_content', 'binary', 'entry_point'));

    final value = Uint8List.fromList([0, 1, 2]);

    group('fromJson', () {
        test('expected: return BinaryContent then ', () {
            final json = {
                'BinaryContent': {
                    'value': [0, 1, 2]
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = BinaryContent(value);
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('expected: return BinaryContent then file is empty', () {
            final json = {
                'BinaryContent': {
                    'value': <dynamic>[]
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = BinaryContent(Uint8List.fromList([]));
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('expected: return BinaryContent then each element is dynamic', () {
            final json = {
                'BinaryContent': {
                    'value': <dynamic>[0, 1, 2]
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = BinaryContent(Uint8List.fromList([0, 1, 2]));
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then class key is invalid', () {
            final json = {
                'DummyClass': {
                    'value': 'a'
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid class key': 'BinaryContent',
                };
                expect(actual, expected);
            }
        });
        test('exception: return FileSystemModelExceptionS then value type is invalid', () {
            final json = {
                'BinaryContent': {
                    'value': 'a'
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then exists non int element', () {
            final json = {
                'BinaryContent': {
                    'value': [1, 2, 'a']
                }
            };
            final base = BinaryContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'message': 'exists non int element in iterable.',
                    'element': 'a',
                    'element type': String,
                };
                expect(actual, expected);
            }
        });
    });

}
