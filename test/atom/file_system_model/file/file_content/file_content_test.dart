// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

// ignore_for_file: unused_import

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

// import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['atom', 'file_system_model', 'file', 'file_content']);
    
    // final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_system_model', 'file', 'file_content', 'entry_point'));

    // decode は FileModel で test する.

    final binaryContent = BinaryContent(Uint8List.fromList([0, 1, 2]));
    final stringContent = StringContent('a', utf8);

    group('fromJson', () {
        test('expected: return BinaryContent then ', () {
            final json = {
                'BinaryContent': {
                    'value': binaryContent.value,
                }
            };
            final base = FileContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = binaryContent;
            expect(actual, expected);
        });
        test('expected: return StringContent then ', () {
            final json = stringContent.toJson();
            final base = FileContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = stringContent;
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then ', () {
            final json = {
                'DummyContent': 'dummy'
            };
            final base = FileContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid key set': {'BinaryContent', 'StringContent'},
                };
                expect(actual, expected);
            }
        });
    });

}
