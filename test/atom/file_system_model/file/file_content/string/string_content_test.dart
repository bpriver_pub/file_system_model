// Copyright (C) 2025, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

// ignore_for_file: unused_import

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

// import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['atom', 'file_system_model', 'file', 'file_content']);
    
    // final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_system_model', 'file', 'file_content', 'binary', 'entry_point'));

    final value = 'a';
    final encoding = utf8;

    group('fromJson', () {
        test('expected: return StringContent then ', () {
            final json = {
                'StringContent': {
                    'value': value,
                    'encoding': {
                        '_Encoding': {
                            'value': encoding.name
                        }
                    },
                }
            };
            final base = StringContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = StringContent(value);
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then class key is invalid', () {
            final json = {
                'DummyClass': {
                    'value': value,
                    'encoding': encoding.name,
                }
            };
            final base = StringContent.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid class key': 'StringContent',
                };
                expect(actual, expected);
            }
        });
    });
    
    group('toJson', () {
        test('expected: return Map<String, Object> then ', () {
            final base = StringContent(value, encoding);
            final result = base.toJson();
            final actual = result;
            final expected = {
                'StringContent': {
                    'value': value,
                    'encoding': {
                        '_Encoding': {
                            'value': encoding.name
                        }
                    },
                }
            };
            expect(actual, expected);
        });
    });

}
