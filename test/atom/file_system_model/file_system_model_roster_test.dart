// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';
import 'package:yaml/yaml.dart' as yaml;

// import 'package:bpriver_debug/bpriver_debug.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['atom', 'file_system_model', 'file_system_model_roster']);

    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'file_system_model_roster'));
    final location = testFolder.path;

    final nameA = 'a';
    final contentA = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentD = fileA.path;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    final nameDummy = 'dummy';

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentA);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentD);
        folderLinkE.createSync(contentE);

    });

    group('FileSystemModelRoster.empty', () {
        test('expected: return FileSystemModelRoster empty values', () {
            final base = const FileSystemModelRoster.empty();
            final actual = base;
            final expected = FileSystemModelRoster(<FileSystemModel>[]);
            expect(actual, expected);
        });
    });

    // 下記の method の test も兼ねる.
    // FileSystemModelRoster._sortByAlphabet
    // FileSystemModelRoster._
    group('FileSystemModelRoster', () {
        test('expected: return FileSystemModelRoster then TreatedAsFolderModel, TreatedAsFileModel, TreatedAsNoneModel, の順で、それらが、名前順にソートされている', () {
            final treatedAsFileModelA = FileLinkModel('a', '');
            final treatedAsFileModelB = FileModel('b', '');
            final treatedAsFileModelC = FileLinkModel('c', '');
            final treatedAsFileModelD = FileModel('d', '');
            final treatedAsFileModelE = FileLinkModel('e', '');
            final treatedAsFileModelF = FileModel('f', '');

            final treatedAsNoneModelG = EmptyLinkModel('g', '');
            final treatedAsNoneModelH = EmptyLinkModel('h', '');
            final treatedAsNoneModelI = EmptyLinkModel('i', '');

            final treatedAsFolderModelJ = FolderLinkModel('j', '');
            final treatedAsFolderModelK = FolderModel('k', FileSystemModelRoster.empty());
            final treatedAsFolderModelL = FolderLinkModel('l', '');
            final treatedAsFolderModelM = FolderModel('m', FileSystemModelRoster.empty());
            final treatedAsFolderModelN = FolderLinkModel('n', '');
            final treatedAsFolderModelO = FolderModel('o', FileSystemModelRoster.empty());

            final Iterable<FileSystemModel> values = [
                // TreatedAsFileModel
                treatedAsFileModelD,
                treatedAsFileModelE,
                treatedAsFileModelF,
                treatedAsFileModelA,
                treatedAsFileModelB,
                treatedAsFileModelC,
                // TreatedAsNoneModel
                treatedAsNoneModelI,
                treatedAsNoneModelG,
                treatedAsNoneModelH,
                // TreatedAsFolderModel
                treatedAsFolderModelM,
                treatedAsFolderModelN,
                treatedAsFolderModelO,
                treatedAsFolderModelJ,
                treatedAsFolderModelK,
                treatedAsFolderModelL,
            ];
            final base = FileSystemModelRoster(values);
            final result = base;
            final actual = result;
            final expected = [
                // TreatedAsFolderModel
                treatedAsFolderModelJ,
                treatedAsFolderModelK,
                treatedAsFolderModelL,
                treatedAsFolderModelM,
                treatedAsFolderModelN,
                treatedAsFolderModelO,
                // TreatedAsFileModel
                treatedAsFileModelA,
                treatedAsFileModelB,
                treatedAsFileModelC,
                treatedAsFileModelD,
                treatedAsFileModelE,
                treatedAsFileModelF,
                // TreatedAsNoneModel
                treatedAsNoneModelG,
                treatedAsNoneModelH,
                treatedAsNoneModelI,
            ];
            expect(actual, expected);
        });
    });

    group('result', () {
        test('expected: return FileSystemModelRoster then ', () {
            final fileSystemModelA = FileModel(nameA, contentA);
            final fileSystemModelB = FileModel(nameB, contentA);
            final fileSystemModelC = FileModel(nameC, contentA);
            final values = <FileModel>[fileSystemModelA, fileSystemModelB, fileSystemModelC];
            final base = FileSystemModelRoster.result<FileModel>(values);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelRoster<FileModel>(values);
            expect(actual, expected);
        });
        test('exception: return RosterPatternExceptionA then duplicate name', () {
            final fileSystemModelA = FileModel(nameA, contentA);
            final values = [fileSystemModelA, fileSystemModelA];
            final base = FileSystemModelRoster.result(values);
            final result = base;
            final actual = result.wrapped;
            final expected = RosterPatternExceptionA();
            expect(actual, expected);
        });
    });

    group('fromYamlMap', () {
        final yamlString = '''
lib:
    src:
        atom:
            atom_layer.dart: ''
test:
    atom:
    compound:
    particle:
pubspec.yaml: |
    name: {applicationName.snake}_{moduleName.snake}_module_module
    description: this package is {applicationName.snake}_{moduleName.snake}_module_module.

    environment:
        sdk: '>=3.0.0 <4.0.0'

    dependencies:
        bpriver_origin:
            path: ../../bpriver_origin

    dev_dependencies:
        lints: ^2.0.0
        test: ^1.20.0
        bpriver_debug:
            path: ../../bpriver_debug

README.md: |
    {applicationName.snake}_{moduleName.snake}_module_module
empty_folder: null
x: true
y: 1
''';
        final yamlMap = yaml.loadYamlDocument(yamlString).contents as yaml.YamlMap;
        test('expected: return FileSystemModelRoster', () {
            final base = FileSystemModelRoster.fromYamlMap(yamlMap);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelRoster([
                FolderModel('lib', FileSystemModelRoster([
                    FolderModel('src', FileSystemModelRoster([
                        FolderModel('atom', FileSystemModelRoster([
                            FileModel('atom_layer.dart', ''),
                        ])),
                    ])),
                ])),
                FolderModel('test', FileSystemModelRoster([
                    FolderModel('atom'),
                    FolderModel('compound'),
                    FolderModel('particle'),
                ])),
                FileModel('pubspec.yaml', '''
name: {applicationName.snake}_{moduleName.snake}_module_module
description: this package is {applicationName.snake}_{moduleName.snake}_module_module.

environment:
    sdk: '>=3.0.0 <4.0.0'

dependencies:
    bpriver_origin:
        path: ../../bpriver_origin

dev_dependencies:
    lints: ^2.0.0
    test: ^1.20.0
    bpriver_debug:
        path: ../../bpriver_debug
'''),
                FileModel('README.md', '''
{applicationName.snake}_{moduleName.snake}_module_module
'''),
                FolderModel('empty_folder'),
                FileModel('x', 'true'),
                FileModel('y', '1'),
            ]);
            // debug.writeLoggerResult(result);
            // debug.winMerge(actual, expected);
            expect(actual, expected);
        });
    });

    group('fromJson', () {
        test('expected: return FileSystemModelRoster then values is empty', () {
            final json = {
                'FileSystemModelRoster': {
                    'values': <Object>[],
                }
            };
            final base = FileSystemModelRoster.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelRoster.empty();
            expect(actual, expected);
        });
        test('expected: return FileSystemModelRoster then values is not empty', () {
            final fileSystemModelRoster = FileSystemModelRoster([
                FileModel('a', 'x', utf8),
                FolderModel('b', FileSystemModelRoster.empty()),
                EmptyLinkModel('c', 'y'),
                FileModel.uint8List('d', Uint8List.fromList('xxx'.codeUnits)),
            ]);
            final json = fileSystemModelRoster.toJson();
            // // print(json);
            // final jsonString = jsonEncode(json);
            // print(jsonString);
            // final jsonB = jsonDecode(jsonString) as Map<String, dynamic>;
            // print(jsonB);
            final base = FileSystemModelRoster.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = fileSystemModelRoster;
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
    });

    group('changeExhaustive', () {
        test('expected: return FileSystemModelRoster', () {
            final base = FileSystemModelRoster([
                FileModel('a', 'a'),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel('f', 'f'),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]);
            final result = base.changeExhaustive(
                (old) {
                    switch (old) {
                    case FileModel():
                        final content = old.content;
                        switch (content) {
                        case StringContent(): return FileModel(old.name + '_a', content.value + content.value);
                        case BinaryContent(): return FileModel.uint8List(old.name + '_a', Uint8List.fromList(content.value + content.value));
                        }
                    case FileLinkModel():
                        return FileLinkModel(old.name + '_b', old.content + old.content);
                    case FolderModel():
                        return FolderModel(old.name + '_c', FileSystemModelRoster([
                            ...old.content,
                            FileModel('g', 'g'),
                        ]));
                    case FolderLinkModel():
                        return FolderLinkModel(old.name + '_d', old.content + old.content);
                    case EmptyLinkModel():
                        return EmptyLinkModel(old.name + '_e', old.content + old.content);
                    }
                }
            );
            final actual = result.wrapped;
            final expected = FileSystemModelRoster([
                FileModel('a_a', 'aa'),
                FileLinkModel('b_b', 'bb'),
                FolderModel('c_c', FileSystemModelRoster([
                    FileModel('f_a', 'ff'),
                    FileModel('g_a', 'gg'),
                ])),
                FolderLinkModel('d_d', 'dd'),
                EmptyLinkModel('e_e', 'ee'),
            ]);
            expect(actual, expected);
        });
    });

    group('createAsComplement()', () {
        final fileANewContent = 'file a new content';
        final fileXName = 'x';
        final fileXContent = 'xxx';
        final fileX = io.File(p.join(location, fileXName));
        final folderYName = 'y';
        final folderY = io.Directory(p.join(location, folderYName));
        final fileModelX = FileModel(fileXName, fileXContent);
        final folderModelY = FolderModel(folderYName, FolderModelRoster.empty());
        test('expected: return Complete then 既存の file が存在しない場合 すべての file が生成される.', () async {
            final base = FileSystemModelRoster([
                fileModelX,
                folderModelY,
            ]);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileX.existsSync(), true);
            expect(fileX.readAsStringSync(), fileXContent);
            expect(folderY.existsSync(), true);
        });
        test('expected: return Complete then 既存の file が存在する場合 存在しない file のみが生成される(上書きされない).', () async {
            final base = FileSystemModelRoster([
                FileModel(nameA, fileANewContent),
                fileModelX,
                folderModelY,
            ]);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileA.readAsStringSync(), contentA);
            expect(fileX.existsSync(), true);
            expect(fileX.readAsStringSync(), fileXContent);
            expect(folderY.existsSync(), true);
        });
    });

    group('createAsOverwrite()', () {
        final fileANewContent = 'file a new content';
        final fileXName = 'x';
        final fileXContent = 'xxx';
        final fileX = io.File(p.join(location, fileXName));
        final folderYName = 'y';
        final folderY = io.Directory(p.join(location, folderYName));
        final fileModelX = FileModel(fileXName, fileXContent);
        final folderModelY = FolderModel(folderYName, FolderModelRoster.empty());
        test('expected: return Complete then 既存の file が存在しない場合 すべての file が生成される.', () async {
            final base = FileSystemModelRoster([
                fileModelX,
                folderModelY,
            ]);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            // debug.writeLoggerResult(result);
            expect(actual, expected);
            expect(fileX.existsSync(), true);
            expect(fileX.readAsStringSync(), fileXContent);
            expect(folderY.existsSync(), true);
        });
        test('expected: return Complete then 既存の file が存在する場合 既存の file は上書きされ すべての file が生成される.', () async {
            final base = FileSystemModelRoster([
                FileModel(nameA, fileANewContent),
                fileModelX,
                folderModelY,
            ]);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileA.readAsStringSync(), fileANewContent);
            expect(fileX.existsSync(), true);
            expect(fileX.readAsStringSync(), fileXContent);
            expect(folderY.existsSync(), true);
        });
    });

    group('existsAsExhaustive()', () {
        final fileModelA = FileModel(nameA, contentA);
        final folderModelB = FolderModel(nameB);
        final emptyLinkModelC = EmptyLinkModel(nameC, contentC);
        final fileLinkModelD = FileLinkModel(nameD, contentD);
        final folderLinkModelE = FolderLinkModel(nameE, contentE);
        final fileModelDummy = FileModel(nameDummy);
        final values = [
            fileModelA,
            folderModelB,
            emptyLinkModelC,
            fileLinkModelD,
            folderLinkModelE,
        ];
        test('expected: return true then すべての FileSystemEntity が実際に存在する', () async {
            final base = FileSystemModelRoster(values);
            final result = await base.existsExhaustive(location);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return false then 何かが一つでも存在しない', () async {
            final base = FileSystemModelRoster([
                ...values,
                fileModelDummy,
            ]);
            final result = await base.existsExhaustive(location);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
        });
    });

    group('synchronize', () {
        final fileX = io.File(p.join(testFolder.path, 'x'));
        final fileY = io.File(p.join(testFolder.path, 'y'));
        final fileZ = io.File(p.join(testFolder.path, 'z'));
        final fileDummy = io.File(p.join(testFolder.path, 'dummy'));
        final values = [
            FileModel('x', 'x new content'),
            FileModel('y', 'y new content'),
            FileModel('z', 'z new content'),
        ];
        test('expected: return Complete then location に file system が存在する場合 それらを削除し すべて新規作成する.', () async {
            fileX.createSync();
            fileX.writeAsStringSync('x content');
            fileY.createSync();
            fileY.writeAsStringSync('y content');
            fileZ.createSync();
            fileZ.writeAsStringSync('z content');
            fileDummy.createSync();
            fileDummy.writeAsStringSync('dummy content');
            final base = FileSystemModelRoster(values);
            final result = await base.synchronize(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileX.readAsStringSync(), 'x new content');
            expect(fileY.readAsStringSync(), 'y new content');
            expect(fileZ.readAsStringSync(), 'z new content');
            expect(fileDummy.existsSync(), false);
            {
                final actual = testFolder.listSync().length;
                final expected = 3;
                expect(actual, expected);
            }
        });
    });

    group('create', () {
        test('expected: return Complete then GenerateMode is ComplementMode', () async {
            final temp = testFolder.createTempSync('a');
            final fileA = io.File(p.join(temp.path, 'a'));
            final fileB = io.File(p.join(temp.path, 'b'));
            final fileC = io.File(p.join(temp.path, 'c'));
            fileA.createSync();
            fileA.writeAsStringSync('a');
            fileB.createSync();
            fileB.writeAsStringSync('b');
            fileC.createSync();
            fileC.writeAsStringSync('c');

            final location = temp.path;
            final generateMode = ComplementMode();
            final base = FileSystemModelRoster(<FileSystemModel>[
                FileModel('a', 'a content'),
                FileModel('b', 'b content'),
                FileModel('c', 'c content'),
                FileModel('d', 'd content'),
            ]);
            final result = await base.create(location, generateMode);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(fileA.readAsStringSync(), 'a');
            expect(fileB.readAsStringSync(), 'b');
            expect(fileC.readAsStringSync(), 'c');
            expect(io.File(p.join(temp.path, 'd')).readAsStringSync(), 'd content');
        });
        test('expected: return Complete then GenerateMode is OverwriteMode', () async {
            final temp = testFolder.createTempSync('b');
            final fileA = io.File(p.join(temp.path, 'a'));
            final fileB = io.File(p.join(temp.path, 'b'));
            final fileC = io.File(p.join(temp.path, 'c'));
            fileA.createSync();
            fileA.writeAsStringSync('a');
            fileB.createSync();
            fileB.writeAsStringSync('b');
            fileC.createSync();
            fileC.writeAsStringSync('c');

            final location = temp.path;
            final generateMode = OverwriteMode();
            final base = FileSystemModelRoster(<FileSystemModel>[
                FileModel('a', 'a content'),
                FileModel('b', 'b content'),
                FileModel('c', 'c content'),
                FileModel('d', 'd content'),
            ]);
            final result = await base.create(location, generateMode);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(fileA.readAsStringSync(), 'a content');
            expect(fileB.readAsStringSync(), 'b content');
            expect(fileC.readAsStringSync(), 'c content');
            expect(io.File(p.join(temp.path, 'd')).readAsStringSync(), 'd content');
        });
        test('expected: return Complete then SynchronizeMode is OverwriteMode', () async {
            final temp = testFolder.createTempSync('c');
            final fileA = io.File(p.join(temp.path, 'a'));
            final fileB = io.File(p.join(temp.path, 'b'));
            final fileC = io.File(p.join(temp.path, 'c'));
            final fileD = io.File(p.join(temp.path, 'd'));
            fileA.createSync();
            fileA.writeAsStringSync('a');
            fileB.createSync();
            fileB.writeAsStringSync('b');
            fileC.createSync();
            fileC.writeAsStringSync('c');
            fileD.createSync();
            fileD.writeAsStringSync('d');

            final location = temp.path;
            final generateMode = SynchronizeMode();
            final base = FileSystemModelRoster(<FileSystemModel>[
                FileModel('a', 'a content'),
                FileModel('b', 'b content'),
                FileModel('c', 'c content'),
            ]);
            final result = await base.create(location, generateMode);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);

            expect(fileA.readAsStringSync(), 'a content');
            expect(fileB.readAsStringSync(), 'b content');
            expect(fileC.readAsStringSync(), 'c content');
            expect(fileD.existsSync(), false);
        });
    });

    group('decode', () {
        test('expected: return FileSystemModelRoster then content is string', () {
            final base = FileSystemModelRoster([
                FileModel.uint8List('a', utf8.encode('a')),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel.uint8List('f', utf8.encode('f')),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]);
            final result = base.decode();
            final actual = result.wrapped;
            final expected = FileSystemModelRoster([
                FileModel('a', 'a'),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel('f', 'f'),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]);
            expect(actual, expected);
        });
        test('exception: return FormatException then content contains invalid byte', () {
            final base = FileSystemModelRoster([
                FileModel.uint8List('a', Uint8List.fromList([10000000])),
            ]);
            final result = base.decode(utf8);
            final actual = result.wrapped;
            final expected = isA<FormatException>();
            expect(actual, expected);
        });
    });

}
