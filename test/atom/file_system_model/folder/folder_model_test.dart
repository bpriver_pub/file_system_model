// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

// import 'package:bpriver_debug/bpriver_debug.dart';

void main() {

    // ignore: unused_local_variable
    // final debug = BpriverDebug(testLocation: ['folder_model_test']);

    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'folder_model'));
    final location = testFolder.path;

    final nameA = 'a';
    final contentA = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentD = fileA.path;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    // final nameDummy = 'dummy';

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentA);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentD);
        folderLinkE.createSync(contentE);

    });

    group('fromPath', () {
        final folderRoot = io.Directory(p.join(location, 'root'));
        final folderA = io.Directory(p.join(folderRoot.path, 'a'));
        final folderB = io.Directory(p.join(folderRoot.path, 'b'));
        final folderC = io.Directory(p.join(folderRoot.path, 'c'));
        final folderCD = io.Directory(p.join(folderRoot.path, 'c', 'd'));
        final folderCE = io.Directory(p.join(folderRoot.path, 'c', 'e'));
        final folderCF = io.Directory(p.join(folderRoot.path, 'c', 'f'));
        final folderCEG = io.Directory(p.join(folderRoot.path, 'c', 'e', 'g'));
        final folderCEH = io.Directory(p.join(folderRoot.path, 'c', 'e', 'h'));
        final folderCEI = io.Directory(p.join(folderRoot.path, 'c', 'e', 'i'));
        final fileA = io.File(p.join(folderRoot.path, 'a.dart'));
        final fileBB = io.File(p.join(folderRoot.path, 'b', 'b.dart'));
        final fileCC = io.File(p.join(folderRoot.path, 'c', 'c.dart'));
        final fileCEII = io.File(p.join(folderRoot.path, 'c', 'e', 'i', 'i.dart'));
        final fileCEIIX = io.File(p.join(folderRoot.path, 'c', 'e', 'i', 'i_x.dart'));
        final subFolders = [ folderA, folderB, folderC, folderCD, folderCE, folderCF, folderCEG, folderCEH, folderCEI,];
        final subFiles = [ fileA, fileBB, fileCC, fileCEII, fileCEIIX, ];
        setUp(() {
            folderRoot.createSync();
            for (final e in subFolders) { e.createSync(); }
            for (final e in subFiles) { e.createSync(); }
        });
        test('expected: return FolderModel then ', () async {
            final base = await FolderModel.fromPath(folderRoot.path);
            final actual = (base.wrapped as FolderModel).decode().wrapped;
            final expected = FolderModel('root', FileSystemModelRoster([
                FolderModel('a', FileSystemModelRoster.empty()),
                FolderModel('b', FileSystemModelRoster([
                    FileModel('b.dart', ''),
                ])),
                FolderModel('c', FileSystemModelRoster([
                    FileModel('c.dart', ''),
                    FolderModel('d', FileSystemModelRoster.empty()),
                    FolderModel('e', FileSystemModelRoster([
                        FolderModel('g', FileSystemModelRoster.empty()),
                        FolderModel('h', FileSystemModelRoster.empty()),
                        FolderModel('i', FileSystemModelRoster([
                            FileModel('i.dart', ''),
                            FileModel('i_x.dart', ''),
                        ])),
                    ])),
                    FolderModel('f', FileSystemModelRoster.empty()),
                ])),
                FileModel('a.dart', ''),
            ]));
            // debug.winMerge(actual, expected);
            expect(actual, expected);
        });
        test('ignore: を指定することにより条件に当てはまった file, folder, link, は file system model として 作成しない.', () async {
            // FileSystemModel.fromPath にも ignore があるが
            // FolderModel.fromPath に渡してるだけなので それらの test は省略.
            final result = await FolderModel.fromPath(
                folderRoot.path,
                choice: (fileSystemEntity) {
                    final log = Log(functionLocation: 'choice');
                    late final bool result;
                    final name = p.basename(fileSystemEntity.path);
                    final containsResult = name.contains('.dart');
                    if (containsResult) {
                        result = false;
                    } else {
                        result = true;
                    }
                    log.monitor({'value': result});
                    return Safety(result, log);
                }
            );
            final actual = result.wrapped;
            final expected = FolderModel('root', FileSystemModelRoster([
                FolderModel('a', FileSystemModelRoster.empty()),
                FolderModel('b', FileSystemModelRoster([
                    // FileModel('b.dart', ''),
                ])),
                FolderModel('c', FileSystemModelRoster([
                    // FileModel('c.dart', ''),
                    FolderModel('d', FileSystemModelRoster.empty()),
                    FolderModel('e', FileSystemModelRoster([
                        FolderModel('g', FileSystemModelRoster.empty()),
                        FolderModel('h', FileSystemModelRoster.empty()),
                        FolderModel('i', FileSystemModelRoster([
                            // FileModel('i.dart', ''),
                            // FileModel('i_x.dart', ''),
                        ])),
                    ])),
                    FolderModel('f', FileSystemModelRoster.empty()),
                ])),
                // FileModel('a.dart', ''),
            ]));
            // debug.writeLoggerResult(result);
            // debug.winMerge(actual, expected);
            expect(actual, expected);
        });
    });

    group('fromJson', () {
        final name = 'a';
        test('expected: return FolderModel then content is empty', () {
            final json = {
                'FolderModel': {
                    'name': name,
                    'content': {
                        'FileSystemModelRoster': {
                            'values': <Object>[],
                        }
                    },
                }
            };
            final base = FolderModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderModel(name, FileSystemModelRoster.empty());
            expect(actual, expected);
        });
        test('expected: return FolderModel then 入れ子になっている file model の string content をちゃんと取得できるか.', () {
            final folder = FolderModel('a', FileSystemModelRoster([
                FileModel('a', ''),
                // FileModel('b', 'xxx'),
                // FileModel('c', 'xyz'),
            ]));
            final json = folder.toJson();
            final base = FolderModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = folder;
            // debug.writeLoggerResult(result);
            expect(actual, expected);
        });
        test('expected: return FolderModel then values is not empty', () {
            final json = {
                'FolderModel': {
                    'name': name,
                    'content': {
                        'FileSystemModelRoster': {
                            'values': [
                                {
                                    'FolderModel': {
                                        'name': 'a',
                                        'content': {
                                            'FileSystemModelRoster': {
                                                'values': [
                                                    {
                                                        'FileModel': {
                                                            'name': 'b',
                                                            'content': {
                                                                'BinaryContent': {
                                                                    'value': [0, 1, 2],
                                                                }
                                                            },
                                                        },
                                                    }
                                                ],
                                            }
                                        },
                                    },
                                },
                                {
                                    'EmptyLinkModel': {
                                        'name': 'c',
                                        'content': 'y',
                                    },
                                }
                            ],
                        }
                    },
                }
            };
            final base = FolderModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderModel(name, FileSystemModelRoster([
                FolderModel('a', FileSystemModelRoster([
                    FileModel.uint8List('b', Uint8List.fromList([0, 1, 2])),
                ])),
                EmptyLinkModel('c', 'y'),
            ]));
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then class key is invalid', () {
            final json = {
                'DummyClass': {
                    'name': name,
                    'content': {
                        'FileSystemModelRoster': {
                            'values': <Object>[],
                        }
                    },
                }
            };
            final base = FolderModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid class key': 'FolderModel',
                };
                expect(actual, expected);
            }
        });
    });

    group('changeExhaustive', () {
        // 大部分は FileSystemModelRoster.changeExhaustive() の test から拝借.
        test('expected: return FolderModel', () {
            final base = FolderModel('root', FileSystemModelRoster([
                FileModel('a', 'a'),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel('f', 'f'),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]));
            final result = base.changeExhaustive(
                (old) {
                    switch (old) {
                    case FileModel():
                        final content = old.content;
                        switch (content) {
                        case StringContent(): return FileModel(old.name + '_a', content.value + content.value);
                        case BinaryContent(): return FileModel.uint8List(old.name + '_a', Uint8List.fromList(content.value + content.value));
                        }
                    case FileLinkModel():
                        return FileLinkModel(old.name + '_b', old.content + old.content);
                    case FolderModel():
                        return FolderModel(old.name + '_c', FileSystemModelRoster([
                            ...old.content,
                            FileModel('g', 'g'),
                        ]));
                    case FolderLinkModel():
                        return FolderLinkModel(old.name + '_d', old.content + old.content);
                    case EmptyLinkModel():
                        return EmptyLinkModel(old.name + '_e', old.content + old.content);
                    }
                }
            );
            final actual = result.wrapped;
            final expected = FolderModel('root_c', FileSystemModelRoster([
                FileModel('a_a', 'aa'),
                FileLinkModel('b_b', 'bb'),
                FolderModel('c_c', FileSystemModelRoster([
                    FileModel('f_a', 'ff'),
                    FileModel('g_a', 'gg'),
                ])),
                FolderLinkModel('d_d', 'dd'),
                FileModel('g_a', 'gg'),
                EmptyLinkModel('e_e', 'ee'),
            ]));
            // debug.winMerge(actual, expected);
            expect(actual, expected);
        });
    });

    group('createAsComplement', () {
        final folderXName = 'x';
        final folderX = io.Directory(p.join(location, folderXName));
        final fileXYName = 'xy';
        final fileXYContent = 'xy content';
        final fileXY = io.File(p.join(folderX.path, fileXYName));
        final fileModelXY = FileModel(fileXYName, fileXYContent);
        final fileXZName = 'xz';
        final fileXZContent = 'xz content';
        final fileXZ = io.File(p.join(folderX.path, fileXZName));
        final fileModelXZ = FileModel(fileXZName, fileXZContent);
        final fileXWName = 'xw';
        final fileXWContent = 'xw content';
        final fileXWNewContent = 'xw new content';
        final fileXW = io.File(p.join(folderX.path, fileXWName));
        final fileModelXW = FileModel(fileXWName, fileXWNewContent);
        final folderXModel = FolderModel(folderXName, FileSystemModelRoster([
            fileModelXY,
            fileModelXZ,
            fileModelXW,
        ]));
        setUp(() {
            if (folderX.existsSync()) folderX.deleteSync(recursive: true);
        });
        test('expected: return Complete then folder が既に存在する場合 その中身に対して 存在しないものを 作成する.', () async {
            folderX.createSync();
            fileXW.createSync();
            fileXW.writeAsStringSync(fileXWContent);
            final base = folderXModel;
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWContent);
        });
        test('expected: return Complete then folder が存在しない場合 folder を作成しその中身に対して 存在しないものを 作成する.', () async {
            final base = folderXModel;
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
        });
        test('expected: return Complete then folder 以外のものが存在する場合何もしない.', () async {
            final fileX = io.File(p.join(location, 'x'));
            fileX.createSync();
            final base = folderXModel;
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileX.existsSync(), true);
            expect(folderX.existsSync(), false);
            expect(fileXY.existsSync(), false);
            expect(fileXZ.existsSync(), false);
            expect(fileXW.existsSync(), false);
        });
    });

    group('createAsOverwrite', () {
        final folderXName = 'x';
        final folderX = io.Directory(p.join(location, folderXName));
        final fileXYName = 'xy';
        final fileXYContent = 'xy content';
        final fileXY = io.File(p.join(folderX.path, fileXYName));
        final fileModelXY = FileModel(fileXYName, fileXYContent);
        final fileXZName = 'xz';
        final fileXZContent = 'xz content';
        final fileXZ = io.File(p.join(folderX.path, fileXZName));
        final fileModelXZ = FileModel(fileXZName, fileXZContent);
        final fileXWName = 'xw';
        final fileXWContent = 'xw content';
        final fileXWNewContent = 'xw new content';
        final fileXW = io.File(p.join(folderX.path, fileXWName));
        final fileModelXW = FileModel(fileXWName, fileXWNewContent);
        final folderXModel = FolderModel(folderXName, FileSystemModelRoster([
            fileModelXY,
            fileModelXZ,
            fileModelXW,
        ]));
        setUp(() {
            if (folderX.existsSync()) folderX.deleteSync(recursive: true);
        });
        test('expected: return Complete then folder が既に存在する場合 その中身に対して 存在するものは上書きして すべて作成する.', () async {
            folderX.createSync();
            fileXW.createSync();
            fileXW.writeAsStringSync(fileXWContent);
            final base = folderXModel;
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
        });
        test('expected: return Complete then folder が存在しない場合 folder を作成しその中身も すべて作成する.', () async {
            final base = folderXModel;
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
        });
        test('expected: return Complete then folder 以外の FileSystem が存在する場合 それを削除して すべて作成する.', () async {
            final fileX = io.File(p.join(location, 'x'));
            fileX.createSync();
            final base = folderXModel;
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
        });
    });

    group('synchronize', () {
        final folderXName = 'x';
        final folderX = io.Directory(p.join(location, folderXName));
        final fileXYName = 'xy';
        final fileXYContent = 'xy content';
        final fileXY = io.File(p.join(folderX.path, fileXYName));
        final fileModelXY = FileModel(fileXYName, fileXYContent);
        final fileXZName = 'xz';
        final fileXZContent = 'xz content';
        final fileXZ = io.File(p.join(folderX.path, fileXZName));
        final fileModelXZ = FileModel(fileXZName, fileXZContent);
        final fileXWName = 'xw';
        final fileXWContent = 'xw content';
        final fileXWNewContent = 'xw new content';
        final fileXW = io.File(p.join(folderX.path, fileXWName));
        final fileModelXW = FileModel(fileXWName, fileXWNewContent);
        final folderXModel = FolderModel(folderXName, FileSystemModelRoster([
            fileModelXY,
            fileModelXZ,
            fileModelXW,
        ]));
        test('expected: return Complete then folder が存在しない場合 folder を作成しその中身も すべて作成する.', () async {
            final base = folderXModel;
            final result = await base.synchronize(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
        });
        test('expected: return Complete then file system が存在する場合 それを削除し folder を作成しその中身も すべて作成する.', () async {
            final fileXV = io.File(p.join(folderX.path, 'v'));
            folderX.createSync();
            fileXW.createSync();
            fileXW.writeAsStringSync(fileXWContent);
            fileXV.createSync();
            final base = folderXModel;
            final result = await base.synchronize(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.readAsStringSync(), fileXYContent);
            expect(fileXZ.readAsStringSync(), fileXZContent);
            expect(fileXW.readAsStringSync(), fileXWNewContent);
            expect(fileXV.existsSync(), false);
        });
    });

    group('existsExhaustive', () {
        final folderXName = 'x';
        final folderX = io.Directory(p.join(location, folderXName));
        final fileXYName = 'xy';
        final fileXYContent = 'xy content';
        final fileXY = io.File(p.join(folderX.path, fileXYName));
        final fileModelXY = FileModel(fileXYName, fileXYContent);
        final fileXZName = 'xz';
        final fileXZContent = 'xz content';
        final fileXZ = io.File(p.join(folderX.path, fileXZName));
        final fileModelXZ = FileModel(fileXZName, fileXZContent);
        final fileXWName = 'xw';
        // final fileXWContent = 'xw content';
        final fileXWNewContent = 'xw new content';
        final fileXW = io.File(p.join(folderX.path, fileXWName));
        final fileModelXW = FileModel(fileXWName, fileXWNewContent);
        final folderXModel = FolderModel(folderXName, FileSystemModelRoster([
            fileModelXY,
            fileModelXZ,
            fileModelXW,
        ]));
        test('expected: return true then すべて存在する', () async {
            folderX.createSync();
            fileXY.createSync();
            fileXZ.createSync();
            fileXW.createSync();
            final base = folderXModel;
            final result = await base.existsExhaustive(location);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.existsSync(), true);
            expect(fileXZ.existsSync(), true);
            expect(fileXW.existsSync(), true);
        });
        test('expected: return false then 一つでも存在しない', () async {
            folderX.createSync();
            fileXY.createSync();
            fileXZ.createSync();
            final base = folderXModel;
            final result = await base.existsExhaustive(location);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
            expect(folderX.existsSync(), true);
            expect(fileXY.existsSync(), true);
            expect(fileXZ.existsSync(), true);
            expect(fileXW.existsSync(), false);
        });
    });

    group('decode', () {
        test('expected: return FolderModel then content is string', () {
            final base = FolderModel('root', FileSystemModelRoster([
                FileModel.uint8List('a', utf8.encode('a')),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel.uint8List('f', utf8.encode('f')),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]));
            final result = base.decode();
            final actual = result.wrapped;
            final expected = FolderModel('root', FileSystemModelRoster([
                FileModel('a', 'a'),
                FileLinkModel('b', 'b'),
                FolderModel('c', FileSystemModelRoster([
                    FileModel('f', 'f'),
                ])),
                FolderLinkModel('d', 'd'),
                EmptyLinkModel('e', 'e'),
            ]));;
            expect(actual, expected);
        });
        test('exception: return FormatException then content contains invalid byte', () {
            final base = FolderModel('root', FileSystemModelRoster([
                FileModel.uint8List('a', Uint8List.fromList([10000000])),
            ]));
            final result = base.decode(utf8);
            final actual = result.wrapped;
            final expected = isA<FormatException>();
            expect(actual, expected);
        });
    });

}
