// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:io' as io;

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'link_model'));
    final location = testFolder.path;

    final nameA = 'a';
    final contentA = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentD = fileA.path;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentA);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentD);
        folderLinkE.createSync(contentE);

    });

    group('fromPath', () {
        test('expected: return EmptyLinkModel then path is empty link.', () async {
            final path = emptyLinkC.path;
            final base = await LinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = EmptyLinkModel(nameC, contentC);
            expect(actual, expected);
        });
        test('expected: return FileLinkModel then path is file link.', () async {
            final path = fileLinkD.path;
            final base = await LinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = FileLinkModel(nameD, contentD);
            expect(actual, expected);
        });
        test('expected: return FolderLinkModel then path is folder link.', () async {
            final path = folderLinkE.path;
            final base = await LinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLinkModel(nameE, contentE);
            expect(actual, expected);
        });
        test('exception: return FileSystemException.', () async {
            final path = folderB.path;
            final base = await LinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'file system entity type': Folder,
            //         'path': path,
            //     };
            //     expect(actual, expected);
            // }
        });
    });

    group('fromJson', () {
        final name = 'a';
        final content = 'x';
        test('expected: return EmptyLinkModel then ', () {
            final json = {
                'EmptyLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = LinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = EmptyLinkModel(name, content);
            expect(actual, expected);
        });
        test('expected: return FileLinkModel then ', () {
            final json = {
                'FileLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = LinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileLinkModel(name, content);
            expect(actual, expected);
        });
        test('expected: return FolderLinkModel then ', () {
            final json = {
                'FolderLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = LinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLinkModel(name, content);
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then ', () {
            final json = {
                'DummyContent': 'dummy'
            };
            final base = LinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid key set': {'EmptyLinkModel', 'FileLinkModel', 'FolderLinkModel'},
                };
                expect(actual, expected);
            }
        });
    });

    group('createAsComplement', () {
        test('expected: return Complete then 既存の link が存在しない場合新規に作成する.', () async {
            final name = 'x';
            final content = 'xxx';
            final link = io.Link(p.join(location, name));
            expect(link.existsSync(), false);
            final base = FileLinkModel(name, content);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(link.existsSync(), true);
            expect(link.targetSync(), content);
        });
        test('expected: return Complete then 既存の link が存在する場合何もしない.', () async {
            final fileLinkDNewContent = 'xxx';
            final base = FileModel(nameD, fileLinkDNewContent);
            final result = await base.createAsComplement(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileLinkD.existsSync(), true);
            expect(fileLinkD.targetSync(), contentD);
        });
    });

    group('createAsOverwrite', () {
        test('expected: return Complete then 既存の file が存在しない場合新規に作成する.', () async {
            final name = 'x';
            final content = 'xxx';
            final link = io.Link(p.join(location, name));
            expect(link.existsSync(), false);
            final base = FileLinkModel(name, content);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(link.existsSync(), true);
            expect(link.targetSync(), content);
        });
        test('expected: return Complete then 既存の file が存在する場合上書きする.', () async {
            final fileLinkDNewContent = 'xxx';
            final base = FileLinkModel(nameD, fileLinkDNewContent);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(fileLinkD.existsSync(), true);
            expect(fileLinkD.targetSync(), fileLinkDNewContent);
        });
        test('expected: return Complete then 既存の link 以外の file system が存在する場合 それを削除し 新規作成する.', () async {
            final link = io.Link(folderB.path);
            expect(link.existsSync(), false);
            final content = 'xxx';
            final base = FileLinkModel(nameB, content);
            final result = await base.createAsOverwrite(location);
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
            expect(folderB.existsSync(), false);
            expect(link.existsSync(), true);
            expect(link.targetSync(), content);
        });
    });

    group('target', () {
        test('expected: return String', () async {
            final base = FileLinkModel(nameD, 'xxx');
            final result = await base.target(location);
            final actual = result.wrapped;
            final expected = contentD;
            expect(actual, expected);
        });
    });

    group('decode', () {
        test('expected: return LinkModel then ', () {
            final base = FileLinkModel(nameD, 'xxx');
            final result = base.decode();
            final actual = result.wrapped;
            final expected = FileLinkModel(nameD, 'xxx');;
            expect(actual, expected);
        });
    });

}
