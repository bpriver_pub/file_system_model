// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:io' as io;

import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';

void main() {

    final testFolder = io.Directory(p.join(p.current, '.dart_tool', 'test', 'atom', 'folder_link_model'));
    // final location = testFolder.path;

    final nameA = 'a';
    final contentA = 'x';
    final fileA = io.File(p.join(testFolder.path, nameA));
    
    final nameB = 'b';
    final folderB = io.Directory(p.join(testFolder.path, nameB));

    final nameC = 'c';
    final contentC = 'dummy';
    final emptyLinkC = io.Link(p.join(testFolder.path, nameC));

    final nameD = 'd';
    final contentD = fileA.path;
    final fileLinkD = io.Link(p.join(testFolder.path, nameD));

    final nameE = 'e';
    final contentE = p.join(p.current, folderB.path);
    final folderLinkE = io.Link(p.join(testFolder.path, nameE));

    setUp(() {

        if(testFolder.existsSync()) {
            final list = testFolder.listSync(followLinks: false);
            for (final e in list) {
                switch (e) {
                    case io.Link(): e.deleteSync(recursive: false);
                    case _: e.deleteSync(recursive: true);
                }
            }
        } else {
            testFolder.createSync(recursive: true);
        }

        fileA.createSync();
        fileA.writeAsStringSync(contentA);
        folderB.createSync();
        emptyLinkC.createSync(contentC);
        fileLinkD.createSync(contentD);
        folderLinkE.createSync(contentE);

    });

    group('fromPath', () {
        test('expected: return FolderModel.', () async {
            final path = folderLinkE.path;
            final base = await FolderLinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLinkModel(nameE, contentE);
            expect(actual, expected);
        });
        test('exception: return FileSystemException.', () async {
            final path = folderB.path;
            final base = await FolderLinkModel.fromPath(path);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<io.FileSystemException>();
            expect(actual, expected);
            // {
            //     final actual = result.log.getMonitor();
            //     final expected = {
            //         'path': path,
            //     };
            //     expect(actual, expected);
            // }
        });
    });

    group('fromJson', () {
        final name = 'a';
        final content = 'x';
        test('expected: return FolderLinkModel then ', () {
            final json = {
                'FolderLinkModel': {
                    'name': name,
                    'content': content,
                }
            };
            final base = FolderLinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FolderLinkModel(name, content);
            expect(actual, expected);
        });
        test('exception: return FileSystemModelExceptionS then class key is invalid', () {
            final json = {
                'DummyClass': {
                    'name': name,
                    'content': content,
                }
            };
            final base = FolderLinkModel.fromJson(json);
            final result = base;
            final actual = result.wrapped;
            final expected = FileSystemModelExceptionS();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'valid class key': 'FolderLinkModel',
                };
                expect(actual, expected);
            }
        });
    });

    group('changeExhaustive()', () {
        test('expected: return FolderLinkModel then name editor and file content editor is not null then return both edited', () {
            final base = FolderLinkModel(nameE, contentE);
            final result = base.changeExhaustive(
                (old) {
                    switch (old) {
                    case FolderLinkModel():
                        return FolderLinkModel(old.name + old.name, old.content + old.content);
                    case _:
                        return old;
                    }
                }
            );
            final actual = result.wrapped;
            final expected = FolderLinkModel(
                nameE + nameE,
                contentE + contentE,
            );
            expect(actual, expected);
        });
    });

}
