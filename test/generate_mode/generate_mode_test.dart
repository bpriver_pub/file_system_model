// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:test/test.dart';

void main() {

    group('fromValue', () {
        test('expected: return ComplementMode then value is complement', () {
            final name = 'complement';
            final base = GenerateMode.fromValue(name);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<ComplementMode>();
            expect(actual, expected);
        });
        test('expected: return OverwriteMode then value is overwrite', () {
            final name = 'overwrite';
            final base = GenerateMode.fromValue(name);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<OverwriteMode>();
            expect(actual, expected);
        });
        test('expected: return SynchronizeMode then value is synchronize', () {
            final name = 'synchronize';
            final base = GenerateMode.fromValue(name);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<SynchronizeMode>();
            expect(actual, expected);
        });
        test('exception: return EnumPatternExceptionA then value is empty', () {
            final name = '';
            final base = GenerateMode.fromValue(name);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<EnumPatternExceptionA>();
            expect(actual, expected);
        });
        test('exception: return EnumPatternExceptionA then value is other', () {
            final name = 'aa';
            final base = GenerateMode.fromValue(name);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<EnumPatternExceptionA>();
            expect(actual, expected);
        });
    });

    group('fromType', () {
        test('expected: return ComplementMode then type is ComplementMode', () {
            final base = GenerateMode.fromType<ComplementMode>();
            final result = base;
            final actual = result.wrapped;
            final expected = isA<ComplementMode>();
            expect(actual, expected);
        });
        test('expected: return OverwriteMode then type is OverwriteMode', () {
            final base = GenerateMode.fromType<OverwriteMode>();
            final result = base;
            final actual = result.wrapped;
            final expected = isA<OverwriteMode>();
            expect(actual, expected);
        });
        test('expected: return SynchronizeMode then type is SynchronizeMode', () {
            final base = GenerateMode.fromType<SynchronizeMode>();
            final result = base;
            final actual = result.wrapped;
            final expected = isA<SynchronizeMode>();
            expect(actual, expected);
        });
    });

}
