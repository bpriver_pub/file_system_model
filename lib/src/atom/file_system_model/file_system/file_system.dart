// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

// unix - What is a "pipe" file? - Stack Overflow: "https://stackoverflow.com/questions/40067887/what-is-a-pipe-file"
// Unix domain socket - Wikipedia: "https://en.wikipedia.org/wiki/Unix_domain_socket"
// unixDomainSock は Unix Domain Socket のことっぽい.
// UNIXドメインソケット - Wikipedia: "https://ja.wikipedia.org/wiki/UNIX%E3%83%89%E3%83%A1%E3%82%A4%E3%83%B3%E3%82%BD%E3%82%B1%E3%83%83%E3%83%88"
// Pipe class - dart:io library - Dart API: "https://api.dart/stable/3.2.6/dart-io/Pipe-class.html"

sealed class FileSystem
    with
        AggregationPattern
{

    String get location;
    String get name;
    String get path;

    const FileSystem();

    @override
    Map<String, Object> get properties {
        return {
            'location': location,
            'name': name,
            'path': path,
        };
    }

    static Safety<FileSystem> fromPath(String path) {
        
        final log = Log(classLocation: FileSystem, functionLocation: 'fromPath');

        final location = p.dirname(path);
        final name = switch (location == path) {
            true => '',
            false => p.basename(path),
        };
        
        // followLinks: false にしなければ link として報告しない.
        final type = io.FileSystemEntity.typeSync(path, followLinks: false);
        switch (type) {
            case io.FileSystemEntityType.directory: return Safety(Folder(location, name, path), log);
            case io.FileSystemEntityType.file: return Safety(File(location, name, path), log);
            case io.FileSystemEntityType.link:
                // link 先が存在しない場合 true だと type は notFound を返す(20240709時点).
                final linkType = io.FileSystemEntity.typeSync(path, followLinks: true);
                switch (linkType) {
                    case io.FileSystemEntityType.directory: return Safety(FolderLink(location, name, path), log);
                    case io.FileSystemEntityType.file: return Safety(FileLink(location, name, path), log);
                    case io.FileSystemEntityType.notFound: return Safety(EmptyLink(location, name, path), log);
                    case _: Result.panic(FileSystemModelErrorC(), log.monitor({
                        'path': path,
                        'type': linkType,
                        'isLink': true,
                    }));
                }
            case io.FileSystemEntityType.notFound: return Safety(NotFound(location, name, path), log);
            case io.FileSystemEntityType.pipe: return Safety(Pipe(location, name, path), log);
            case io.FileSystemEntityType.unixDomainSock: return Safety(UnixDomainSocket(location, name, path), log);
            case _: Result.panic(FileSystemModelErrorC(), log.monitor({
                'path': path,
                'type': type,
                'isLink': false,
            }));
        }
    }

    static Safety<FileSystem> fromLocationAndName(String location, String name) {

        final log = Log(classLocation: FileSystem, functionLocation: 'fromLocationAndName');

        final path = p.join(location, name);

        final result = FileSystem.fromPath(path);
        log.add(result);

        return Safety(result.wrapped, log);
    }

    FileSystem change({
        String? location,
        String? name,
    }) {

        // final log = Log(classLocation: runtimeType, functionLocation: 'change');
        
        final newLocation = location ?? this.location;
        final newName = name ?? this.name;
        final newPath = p.join(newLocation, newName);

        final result = FileSystem.fromPath(newPath);
        // log.add(result);

        return result.wrapped;
    }

    String getNewPath({
        String? location,
        String? name,
    }) {
        final newLocation = location ?? this.location;
        final newName = name ?? this.name;
        return p.join(newLocation, newName);
    }

}
