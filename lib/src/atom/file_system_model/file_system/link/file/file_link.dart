// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

final class FileLink
    extends
        Link
{

    final String location;
    final String name;
    final String path;

    const FileLink(this.location, this.name, this.path);

}