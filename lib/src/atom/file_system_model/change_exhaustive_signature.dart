// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template ChangeExhaustiveSignature}
/// 
/// used by [FileSystemModel] and [FileSystemModelRoster].<br>
/// return type is [FileSystemModel] or Iterable<[FileSystemModel]>.<br>
/// 
/// FileSystemModelRoster が RosterPatternExceptionA を返すので Safety ではなく Result.<br>
/// editor 内での Danger に対応するために editor の return type を Danger にする場合 FolderModel での処理が複雑になりすぎるため断念(test がめんどくさいし 呼び出し側も複雑になる.)<br>
/// どうしてもという場合は try catch による exception handling を 呼び出し側で行う.<br>
/// 
/// {@endtemplate}
abstract class ChangeExhaustiveSignature
{

    /// {@macro ChangeExhaustiveSignature}
    Result<Object> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor);

}