// Copyright (C) 2025, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template _Encoding}
/// [Encoding] を wrap するための value pattern class.<br>
/// toJson 時の辻褄合わせに必要.<br>
/// {@endtemplate}
final class _Encoding
    with
        ValuePattern<String>
{

    static const String JSON_KEY_CLASS = '_Encoding';
    static const String JSON_KEY_VALUE = 'value';

    final Encoding _value;

    /// {@macro _Encoding}
    const _Encoding(this._value);

    static Danger<_Encoding, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: _Encoding, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        // toJson() で encoding には String type である encoding name が保存されているはずである.
        final jsonKeyValueResult = jsonKeyClassResult[JSON_KEY_VALUE];
        if (jsonKeyValueResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_VALUE key result': jsonKeyValueResult,
            '$JSON_KEY_VALUE key result type': jsonKeyValueResult.runtimeType,
        }));

        final getByNameResult = Encoding.getByName(jsonKeyValueResult);
        if (getByNameResult == null) return Failure(FileSystemModelExceptionS(), log.monitor({
            'not supported encoding': jsonKeyValueResult,
        }));

        final result = _Encoding(getByNameResult);

        return Success(result, log);

    }

    @override
    String get value => _value.name;

}
