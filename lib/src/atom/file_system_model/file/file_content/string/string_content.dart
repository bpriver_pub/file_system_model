// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template StringContent}
/// 
/// {@endtemplate}
final class StringContent
    extends
        FileContent
{

    static const String JSON_KEY_CLASS = 'StringContent';
    static const String JSON_KEY_VALUE = 'value';
    static const String JSON_KEY_ENCODING = 'encoding';

    final String value;
    final _Encoding _encoding;

    /// {@macro StringContent}
    StringContent(this.value, [Encoding encoding = utf8])
    :
        _encoding = _Encoding(encoding)
    ;

    static Danger<StringContent, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: StringContent, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyValueResult = jsonKeyClassResult[JSON_KEY_VALUE];
        if (jsonKeyValueResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_VALUE key result': jsonKeyValueResult,
            '$JSON_KEY_VALUE key result type': jsonKeyValueResult.runtimeType,
        }));

        // toJson() で encoding には String type である encoding name が保存されているはずである.
        final jsonKeyEncodingResult = jsonKeyClassResult[JSON_KEY_ENCODING];
        if (jsonKeyEncodingResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_ENCODING key result': jsonKeyEncodingResult,
            '$JSON_KEY_ENCODING key result type': jsonKeyEncodingResult.runtimeType,
        }));

        final encodingResult = _Encoding.fromJson(jsonKeyEncodingResult);
        log.add(encodingResult);
        if (encodingResult is! Success<_Encoding, FileSystemModelExceptionS>) return Failure(encodingResult.asException, log);

        final result = StringContent(jsonKeyValueResult, encodingResult.wrapped._value);

        return Success(result, log);

    }

    Encoding get encoding => _encoding._value;

    @override
    Map<String, Object> get properties {
        return {
            'value': value,
            'encoding': _encoding,
        };
    }

}
