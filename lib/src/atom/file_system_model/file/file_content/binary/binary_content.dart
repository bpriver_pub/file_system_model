// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template BinaryContent}
/// 
/// {@endtemplate}
final class BinaryContent
    extends
        FileContent
{

    static const String JSON_KEY_CLASS = 'BinaryContent';
    static const String JSON_KEY_VALUE = 'value';

    final Uint8List value;

    /// {@macro BinaryContent}
    const BinaryContent(this.value);

    static Danger<BinaryContent, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: BinaryContent, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyValueResult = jsonKeyClassResult[JSON_KEY_VALUE];

        // Uint8List は List<int> を implements しているので toJson で List<int> になっている.
        if (jsonKeyValueResult is Iterable<dynamic>) {

            // file が empty の場合 List<dynamic> が返る.
            if (jsonKeyValueResult.isEmpty) return Success(BinaryContent(Uint8List.fromList([])), log);

            final list = <int>[];

            for (final i in jsonKeyValueResult) {

                if (i is! int) return Failure(FileSystemModelExceptionS(), log.monitor({
                    'message': 'exists non int element in iterable.',
                    'element': i,
                    'element type': i.runtimeType,
                }));

                list.add(i);
                
            }

            final fromListResult = Uint8List.fromList(list);
            final result = BinaryContent(fromListResult);

            return Success(result, log);

        }

        return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_VALUE key result': jsonKeyValueResult,
            '$JSON_KEY_VALUE key result type': jsonKeyValueResult.runtimeType,
        }));

    }

    @override
    Map<String, Object> get properties {
        return {
            'value': value,
        };
    }

}
