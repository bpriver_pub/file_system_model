// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template FileContent}
/// 
/// {@endtemplate}
sealed class FileContent
    with
        AggregationPattern
{

    static const Set<String> JSON_KEY_CLASS_SET = {
        BinaryContent.JSON_KEY_CLASS,
        StringContent.JSON_KEY_CLASS,
    };

    /// {@macro FileContent}
    const FileContent();

    static Danger<FileContent, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: FileContent, functionLocation: 'fromJson');

        if (json.containsKey(BinaryContent.JSON_KEY_CLASS)) {
            final result = BinaryContent.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(StringContent.JSON_KEY_CLASS)) {
            final result = StringContent.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid key set': JSON_KEY_CLASS_SET,
        }));

    }

    /// {@template FileContent.decode}
    /// [BinaryContent] を [StringContent] へ変換する.<br>
    /// [StringContent] だった場合 そのまま返す.<br>
    /// {@endtemplate}
    Danger<StringContent, FormatException> decode([Encoding encoding = utf8]) {

        final log = Log(classLocation: runtimeType, functionLocation: 'decode');

        final i = this;

        switch (i) {
        case StringContent(): return Success(i, log);
        case BinaryContent():
            return Danger.tryAndCatch(
                () {
                    final newContent = encoding.decode(i.value);
                    return StringContent(newContent, encoding);
                },
                log,
            );
        }

    }

}
