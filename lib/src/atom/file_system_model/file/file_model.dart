// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template FileModel}
/// 
/// {@endtemplate}
final class FileModel
    extends
        FileSystemModel
    implements
        TreatedAsFileModel
{

    static const String JSON_KEY_CLASS = 'FileModel';
    static const String JSON_KEY_NAME = 'name';
    static const String JSON_KEY_CONTENT = 'content';

    final String name;
    final FileContent content;

    /// {@macro FileModel}
    FileModel(this.name, [String content = '', Encoding encoding = utf8])
    :
        content = StringContent(content, encoding)
    ;

    /// {@macro FileModel}
    FileModel.uint8List(this.name, Uint8List content)
    :
        content = BinaryContent(content)
    ;

    /// {@macro FileModel}
    FileModel.model(this.name, this.content);

    /// {@template FileModel.fromFileSystemEntity}
    /// path から [FileModel] を生成する.<br>
    /// この時 content は 失敗を回避するために [BinaryContent] として生成される.<br>
    /// {@endtemplate}
    static FutureDanger<FileModel, io.FileSystemException> fromPath(String path) async {

        final log = Log(classLocation: FileModel, functionLocation: 'fromPath');

        final fileSystemFromPathResult = FileSystem.fromPath(path);
        log.add(fileSystemFromPathResult);
        final pathStruct = fileSystemFromPathResult.wrapped;

        // error が発生しないので byte で読み取る.
        return await Danger.tryAndCatchFuture(
            () async {
                final file = io.File(path);
                final content = await file.readAsBytes();
                return FileModel.uint8List(pathStruct.name, content);
            },
            log,
        );

    }

    /// {@template FileModel.fromLocationAndName}
    /// この時 content は 失敗を回避するために [BinaryContent] として生成される.<br>
    /// {@endtemplate}
    static FutureDanger<FileModel, io.FileSystemException> fromLocationAndName(String location, String name) async {

        final log = Log(classLocation: FileModel, functionLocation: 'fromLocationAndName');

        final path = p.join(location, name);

        final fromPathResult = await FileModel.fromPath(path);
        log.add(fromPathResult);

        return Danger.fromDanger(fromPathResult, log);

    }

    static Danger<FileModel, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: FileModel, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyNameResult = jsonKeyClassResult[JSON_KEY_NAME];
        if (jsonKeyNameResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_NAME key result': jsonKeyNameResult,
            '$JSON_KEY_NAME key result type': jsonKeyNameResult.runtimeType,
        }));

        final jsonKeyContentResult = jsonKeyClassResult[JSON_KEY_CONTENT];
        if (jsonKeyContentResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_CONTENT key result': jsonKeyContentResult,
            '$JSON_KEY_CONTENT key result type': jsonKeyContentResult.runtimeType,
        }));
        final contentResult = FileContent.fromJson(jsonKeyContentResult);
        log.add(contentResult);
        if (contentResult is! Success<FileContent, FileSystemModelExceptionS>) return Failure(contentResult.asException, log);

        final result = FileModel.model(jsonKeyNameResult, contentResult.wrapped);

        return Success(result, log);

    }

    @override
    Safety<FileSystemModel> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor) {

        final log = Log(classLocation: runtimeType, functionLocation: 'changeExhaustive');

        final editorResult = editor(this);

        return Safety(editorResult, log);

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsComplement(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsComplement');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        return Danger.tryAndCatch(
            () {
                switch (fileSystem) {
                case NotFound():
                    final file = io.File(fileSystem.path);
                    file.createSync(recursive: false, exclusive: false);
                    final i = content;
                    switch (i) {
                    case BinaryContent(): file.writeAsBytesSync(i.value);
                    case StringContent(): file.writeAsStringSync(i.value, encoding: i.encoding);
                    }
                case _:;
                }
                return Complete();
            },
            log,
        );

    }

    /// {@template FileModel.createAsOverwrite}
    /// 
    /// 同名の file が存在する場合 その file は削除せず 内容だけを上書きする.<br>
    /// 同名の file 以外の file system entity が存在する場合 それを削除し file を新規作成する.
    /// 
    /// {@endtemplate}
    @override
    FutureDanger<Complete, io.FileSystemException> createAsOverwrite(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsOverwrite');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;
        
        final file = io.File(fileSystem.path);

        switch (fileSystem) {
        case File():
            // file が既に存在する場合 それを削除せず write だけ実行する
            // delete して create した場合 file 作成日時が新しく変わってしまい 予想外の挙動となってしまうため
            break;
        case Link():
        case Folder():
        case Pipe():
        case UnixDomainSocket():
            // file 以外の何かしらの entity が存在した場合削除して file を新規作成する.
            final deleteResult = await FileSystemModel.delete(fileSystem.path);
            log.add(deleteResult); 
            if (deleteResult is! Success<Complete, io.FileSystemException>) return Failure(deleteResult.asException, log);
            continue createCase;
        createCase:
        case NotFound():
            final Danger<Complete, io.FileSystemException> tryAndCatchResult = await Danger.tryAndCatchFuture(
                () async {
                    file.createSync(recursive: false, exclusive: false);
                    return Complete();
                },
                log,
            );
            if (tryAndCatchResult is! Success<Complete, io.FileSystemException>) return Failure(tryAndCatchResult.asException, log);
        }
        
        final Danger<Complete, io.FileSystemException> tryAndCatchResult = await Danger.tryAndCatchFuture(
            () async {
                final i = content;
                switch (i) {
                case BinaryContent(): file.writeAsBytesSync(i.value);
                case StringContent(): file.writeAsStringSync(i.value, encoding: i.encoding);
                }
                return Complete();
            },
            log,
        );
        if (tryAndCatchResult is! Success<Complete, io.FileSystemException>) return Failure(tryAndCatchResult.asException, log);

        return Success(Complete(), log) ;

    }

    /// {@template FileModel.decode}
    /// [BinaryContent] を [StringContent] へ変換する.
    /// {@endtemplate}
    @override
    Danger<FileModel, FormatException> decode([Encoding encoding = utf8]) {

        final log = Log(classLocation: runtimeType, functionLocation: 'encode');

        final decodeResult = content.decode(encoding);
        log.add(decodeResult);
        if (decodeResult is! Success<StringContent, FormatException>) return Failure(decodeResult.asException, log);

        final result = FileModel.model(name, decodeResult.wrapped);

        return Success(result, log);

    }

}
