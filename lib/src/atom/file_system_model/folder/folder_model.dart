// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

final class FolderModel
    extends
        FileSystemModel
    implements
        TreatedAsFolderModel
        ,ExistsExhaustiveSignature
{

    static const String JSON_KEY_CLASS = 'FolderModel';
    static const String JSON_KEY_NAME = 'name';
    static const String JSON_KEY_CONTENT = 'content';

    final String name;
    final FileSystemModelRoster content;

    FolderModel(this.name, [this.content = const FileSystemModelRoster.empty()]);

    // choice という名前
    //      ignore だと 実際に code を記述する際 true で無視、 false で選択、になるので混乱を招く.
    // root は file, folder, link, ともに ignore の対象外になる.
    // そうしないと処理に null が必要になってしまう.
    // また root を対象外にすることで ignore を使う場面は folder 生成時のみになり folder にだけ ignore を用意すればよくなる.
    // また パフォーマンスの観点から FileSystemModel の生成前に 除外する.
    /// ignore で 生成に含めない [io.FileSystemEntity] の条件を設定できる.
    static FutureDanger<FolderModel, io.FileSystemException> fromPath(String path, {Safety<bool> Function(io.FileSystemEntity fileSystemEntity)? choice}) async {

        final log = Log(classLocation: FolderModel, functionLocation: 'fromPath');

        final fileSystemResult = FileSystem.fromPath(path);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        final Danger<List<io.FileSystemEntity>, io.FileSystemException> subFoldersResult = await Danger.tryAndCatchFuture(
            () async {

                final folder = io.Directory(path);
                final subFolders = folder.listSync(recursive: false, followLinks: false);
                log.monitor({'folder path': folder.path});
                log.monitor({'sub folder length': subFolders.length});

                return subFolders;
            },
            log,
        );
        if (subFoldersResult is! Success<List<io.FileSystemEntity>, io.FileSystemException>) return Failure(subFoldersResult.asException, log);
        final subFolders = subFoldersResult.wrapped;

        List<FileSystemModel> list = [];

        for (final e in subFolders) {

            // choice 処理
            if (choice != null) {
                // choice の条件に当てはまったら その file system entity は追加する.
                final choiceResult = choice(e);
                log.add(choiceResult);
                final value = choiceResult.wrapped;
                // ここで log.monitor をしても意味がない. 上書きされてしまうしmonitor部分で固まるし.
                // log.monitor('value', value);
                // なので choice の中身に log を記述する必要がある. つまり 呼び出し側でやってもらうしかない.
                if(!value) continue;
            }

            final fileSystemModelResult = await FileSystemModel.fromPath(e.path, choice: choice);
            log.add(fileSystemModelResult);
            if (fileSystemModelResult is! Success<FileSystemModel, io.FileSystemException>) return Failure(fileSystemModelResult.asException, log);

            list.add(fileSystemModelResult.asExpected);
        }

        final contentResult = FileSystemModelRoster.result(list);
        log.add(contentResult);
        // 実際の file system entity が対象のため 重複は発生しないので throw out.
        if (contentResult is! Success<FileSystemModelRoster<FileSystemModel>, RosterPatternExceptionA>) Result.panic(FileSystemModelErrorB(), log.monitor({'exception': contentResult.wrapped}));

        final result = FolderModel(fileSystem.name, contentResult.wrapped);

        return Success(result, log);

    }

    static Danger<FolderModel, FileSystemModelExceptionST> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: FolderModel, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyNameResult = jsonKeyClassResult[JSON_KEY_NAME];
        if (jsonKeyNameResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_NAME key result': jsonKeyNameResult,
            '$JSON_KEY_NAME key result type': jsonKeyNameResult.runtimeType,
        }));

        final jsonKeyContentResult = jsonKeyClassResult[JSON_KEY_CONTENT];
        if (jsonKeyContentResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_CONTENT key result': jsonKeyContentResult,
            '$JSON_KEY_CONTENT key result type': jsonKeyContentResult.runtimeType,
        }));

        final fileSystemModelRosterResult = FileSystemModelRoster.fromJson(jsonKeyContentResult);
        log.add(fileSystemModelRosterResult);
        if (fileSystemModelRosterResult is! Success<FileSystemModelRoster<FileSystemModel>, FileSystemModelExceptionST>) return Failure(fileSystemModelRosterResult.asException, log);

        final result = FolderModel(jsonKeyNameResult, fileSystemModelRosterResult.wrapped);

        return Success(result, log);

    }

    FolderModel change({
        String? name,
        FileSystemModelRoster? content,
    }) {
        return FolderModel(
            name ?? this.name,
            content ?? this.content,
        );
    }

    @override
    Danger<FileSystemModel, RosterPatternExceptionA> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor) {

        final log = Log(classLocation: runtimeType, functionLocation: 'changeExhaustive');

        final editorResult = editor(this) as FolderModel;

        final contentChangeExhaustiveResult = editorResult.content.changeExhaustive(editor);
        log.add(contentChangeExhaustiveResult);
        if (contentChangeExhaustiveResult is! Success<FileSystemModelRoster<FileSystemModel>, RosterPatternExceptionA>) return Failure(contentChangeExhaustiveResult.asException, log);

        final result = FolderModel(editorResult.name, contentChangeExhaustiveResult.wrapped);

        return Success(result, log);

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsComplement(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'createAsComplement');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        switch (fileSystem) {
            // folder が既に存在する場合 その中身に対して網羅的に createAsCompelement を実行.
            case Folder():;
            // 何も存在しない場合 folder を作成し その中身に対して網羅的に createAsCompelement を実行.
            case NotFound():
                final Danger<Complete, io.FileSystemException> tryAndCatchResult = Danger.tryAndCatch(
                    () {
                        final folder = io.Directory(fileSystem.path);
                        folder.createSync(recursive: false);
                        return Complete();
                    },
                    log,
                );
                if (tryAndCatchResult is! Success<Complete, io.FileSystemException>) return Failure(tryAndCatchResult.asException, log);
            // folder 以外のものが存在する場合何もしない.
            case _:
                return Success(Complete(), log);
        }

        final contentLocation = p.join(location, name);

        final createAsComplementResult = await content.createAsComplement(contentLocation);
        log.add(createAsComplementResult);
        return Danger.fromDanger(createAsComplementResult, log);

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsOverwrite(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsOverwrite');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        final contentLocation = p.join(location, name);

        switch (fileSystem) {
            // folder が既に存在する場合 その中身に対して網羅的に createAsOverwrite を実行.
            case Folder():
                final createAsOverwriteResult = await content.createAsOverwrite(contentLocation);
                log.add(createAsOverwriteResult);
                return Danger.fromDanger(createAsOverwriteResult, log);
            // folder が存在しない場合 createAsComplement を実行.
            case NotFound():
                final createAsComplementResult = await createAsComplement(fileSystem.location);
                log.add(createAsComplementResult);
                if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createAsComplementResult.asException, log);
                return Danger.fromDanger(createAsComplementResult, log);
            // folder 以外の FileSystem が存在する場合 それを削除して createAsComplement 実行.
            case _:

                final deleteResult = await FileSystemModel.delete(fileSystem.path);
                log.add(deleteResult); 
                if (deleteResult is! Success<Complete, io.FileSystemException>) return Failure(deleteResult.asException, log);

                final createAsComplementResult = await createAsComplement(location);
                log.add(createAsComplementResult);
                if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createAsComplementResult.asException, log);
                return Danger.fromDanger(createAsComplementResult, log);
        }

    }

    /// {@template FolderModel.synchronize}
    /// 
    /// 指定した location 配下の既存の file system entity はすべて削除し 新たに file system を作成する.
    /// 
    /// not fodler file system model の場合 createAsOverwrite と挙動が同じなので FodlerModel と FileSystemModelRoser のみに実装. 
    /// 
    /// {@endtemplate}
    FutureDanger<Complete, io.FileSystemException> synchronize(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'synchronize');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        switch (fileSystem) {
            case NotFound():
                break;
            case _:
                final deleteResult = await FileSystemModel.delete(fileSystem.path);
                log.add(deleteResult);
                if (deleteResult is! Success<Complete, io.FileSystemException>) return Failure(deleteResult.asException, log);
        }

        final createAsComplementResult = await createAsComplement(location);
        log.add(createAsComplementResult);
        if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createAsComplementResult.asException, log);

        return Success(createAsComplementResult.wrapped, log);

    }

    @override
    FutureSafety<bool> existsExhaustive(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'existsAsExhaustive');

        final existsResult = await exists(location);
        log.add(existsResult);

        switch (existsResult.wrapped) {
            case Folder():
                break;
            case _:
                return Safety(false, log);
        }

        final contentLocation = p.join(location, name);

        final existsExhaustiveResult = await content.existsExhaustive(contentLocation);
        log.add(existsExhaustiveResult);

        return Safety(existsExhaustiveResult.wrapped, log);

    }

    /// {@template FolderModel.decode}
    /// [BinaryContent] を [StringContent] へ変換する.
    /// {@endtemplate}
    @override
    Danger<FolderModel, FormatException> decode([Encoding encoding = utf8]) {

        final log = Log(classLocation: runtimeType, functionLocation: 'encode');

        final encodeExhaustiveResult = content.decode(encoding);
        log.add(encodeExhaustiveResult);
        if (encodeExhaustiveResult is! Success<FileSystemModelRoster<FileSystemModel>, FormatException>) return Failure(encodeExhaustiveResult.asException, log);

        final result = FolderModel(name, encodeExhaustiveResult.wrapped);

        return Success(result, log);

    }

}
