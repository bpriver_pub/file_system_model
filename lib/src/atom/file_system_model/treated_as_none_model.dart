// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template TreatedAsNoneModel}
/// 
/// [FileModel] とも [FolderModel] とも扱うわないもの. <br>
/// [EmptyLinkModel]. <br>
/// 
/// {@endtemplate}
sealed class TreatedAsNoneModel
    implements
        FileSystemModel
{}
