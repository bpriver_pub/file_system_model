// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library file_system_model;

import 'dart:convert';
import 'dart:io' as io;
import 'dart:typed_data';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/src/foundation/foundation_layer.dart';
import 'package:path/path.dart' as p;
import 'package:yaml/yaml.dart';

part 'file_system/file_system.dart';
part 'file_system/file/file.dart';
part 'file_system/folder/folder.dart';
part 'file_system/link/link.dart';
part 'file_system/link/empty/empty_link.dart';
part 'file_system/link/file/file_link.dart';
part 'file_system/link/folder/folder_link.dart';
part 'file_system/not_found/not_found.dart';
part 'file_system/pipe/pipe.dart';
part 'file_system/unix_domain_socket/unix_domain_socket.dart';

part 'change_exhaustive_signature.dart';
part 'create_as_complement_signature.dart';
part 'create_as_overwrite_signature.dart';
part 'exists_exhaustive_signature.dart';

part 'file/file_model.dart';
part 'file/file_content/file_content.dart';
part 'file/file_content/binary/binary_content.dart';
part 'file/file_content/string/string_content.dart';
part 'file/file_content/string/encoding/encoding.dart';

part 'folder/folder_model.dart';

part 'link/link_model.dart';
part 'link/file/file_link_model.dart';
part 'link/folder/folder_link_model.dart';
part 'link/empty/empty_link_model.dart';

part 'file_system_model_roster.dart';
part 'file_system_model_typedef.dart';

part 'treated_as_file_model.dart';
part 'treated_as_folder_model.dart';
part 'treated_as_none_model.dart';

sealed class FileSystemModel
    with
        AggregationPattern
        ,EntityPattern<String>
    implements
        ChangeExhaustiveSignature
        ,CreateAsComplementSignature
        ,CreateAsOverwriteSignature
{

    static const Set<String> JSON_KEY_CLASS_SET = {
        FileModel.JSON_KEY_CLASS,
        FolderModel.JSON_KEY_CLASS,
        EmptyLinkModel.JSON_KEY_CLASS,
        FileLinkModel.JSON_KEY_CLASS,
        FolderLinkModel.JSON_KEY_CLASS,
    };
    
    String get name;
    Object get content;

    const FileSystemModel();

    static FutureDanger<FileSystemModel, io.FileSystemException> fromPath(String path, {Safety<bool> Function(io.FileSystemEntity fileSystemEntity)? choice}) async {
        
        final log = Log(classLocation: FileSystemModel, functionLocation: 'fromPath');

        final fileSystemResult = FileSystem.fromPath(path);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        // throwOut...必ず存在するentityに振り分けられるので, その path が指定と実際と乖離することはない.
        switch (fileSystemResult.wrapped) {
        case File():
            final result = await FileModel.fromPath(path);
            log.add(result);
            return Danger.fromDanger(result, log);
        case Folder():
            final result = await FolderModel.fromPath(path, choice: choice);
            log.add(result);
            return Danger.fromDanger(result, log);
        case Link():
            final result = await LinkModel.fromPath(path);
            log.add(result);
            return Danger.fromDanger(result, log);
        case _:
            final message = FileSystemModelExceptionA.LOGGER_RESULT_MESSAGE.join('\n');
            return Failure(io.FileSystemException(message, fileSystem.path), log);
        }

    }

    /// {@template FileSystemModel.rename}
    /// すでに存在する名前を指定した時 既に存在していた file system を上書する.
    /// {@endtemplate}
    static FutureDanger<Complete, io.FileSystemException> rename(String path, String newName) async {

        final log = Log(classLocation: FileSystemModel, functionLocation: 'rename');

        final fileSystemResult = FileSystem.fromPath(path);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        late final io.FileSystemEntity fileSystemEntity;

        switch (fileSystem) {
        case File():
            fileSystemEntity = io.File(fileSystem.path);
        case Folder():
            fileSystemEntity = io.Directory(fileSystem.path);
        case Link():
            fileSystemEntity = io.Link(fileSystem.path);
        case _:
            final message = FileSystemModelExceptionA.LOGGER_RESULT_MESSAGE.join('\n');
            return Failure(io.FileSystemException(message, fileSystem.path), log);
        }

        final newPath = fileSystem.getNewPath(name: newName);

        return await Danger.tryAndCatchFuture(
            () async {
                await fileSystemEntity.rename(newPath);
                return Complete();
            },
            log,
        );

    }

    /// {@template FileSystemModel.delete}
    /// delete file system.<br>
    /// {@endtemplate}
    static FutureDanger<Complete, io.FileSystemException> delete(String path) async {
        
        final log = Log(classLocation: FileSystemModel, functionLocation: 'delete');

        final fileSystemResult = FileSystem.fromPath(path);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        // FileSystemEntity が Link の場合,
        // recursive: true だと 何故か その FileSystemEntity が Directory という扱いになり 'ディレクトリ名が無効です' という OS Error が発生する.
        // なので Directory ではない場合 recursive: false にして 実行しなければならない.
        switch (fileSystem) {
            case Folder():
                final folder = io.Directory(fileSystem.path);
                return Danger.tryAndCatch(
                    () {
                        folder.deleteSync(recursive: true);
                        return Complete();
                    },
                    log,
                );
            case File():
                final file = io.File(fileSystem.path);
                return Danger.tryAndCatch(
                    () {
                        file.deleteSync(recursive: false);
                        return Complete();
                    },
                    log,
                );
            case Link():
                final link = io.Link(fileSystem.path);
                return Danger.tryAndCatch(
                    () {
                        link.deleteSync(recursive: false);
                        return Complete();
                    },
                    log,
                );
            case NotFound():
            case Pipe():
            case UnixDomainSocket():
                final message = FileSystemModelExceptionN.LOGGER_RESULT_MESSAGE.join('\n');
                return Failure(io.FileSystemException(message, fileSystem.path), log);
        }

    }

    /// {@template FileSystemModel.copy}
    /// [copyPath] が file, folder, link, では無い場合 失敗する.<br>
    /// paste 先に folder, pipe, unix domain socket, が既に存在した場合失敗する.<br>
    /// paste 先に file, link, が既に存在した場合 それを削除し paste する.<br>
    /// {@endtemplate}
    static FutureDanger<Complete, io.FileSystemException> copy(String copyPath, String pasteLocation) async {

        final log = Log(classLocation: FileSystemModel, functionLocation: 'copy');

        final fileSystemResult = FileSystem.fromPath(copyPath);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        final newPath = fileSystem.change(location: pasteLocation);

        switch (newPath) {
        // paste先に file, link, が存在する場合 それを削除.
        case File():
        case Link():
            final deleteResult = await FileSystemModel.delete(newPath.path);
            log.add(deleteResult);
            if (deleteResult is! Success<Complete, io.FileSystemException>) return Failure(deleteResult.asException, log);
        // paste先に folder, pipe, unixdomainsocket, が存在する場合 失敗.
        case Folder():
        case Pipe():
        case UnixDomainSocket():
            final message = FileSystemModelExceptionQ.LOGGER_RESULT_MESSAGE.join('\n');
            return Failure(io.FileSystemException(message, fileSystem.path), log);
        // 何もしない.
        case NotFound():
            break;
        }

        switch (fileSystem) {
        case File():
            // dart 公式では copy method は File にしか無い.
            return await Danger.tryAndCatchFuture(
                () async {
                    final file = io.File(copyPath);
                    await file.copy(newPath.path);
                    return Complete();
                },
                log,
            );
        case Folder():
            final folderModelResult = await FolderModel.fromPath(copyPath);
            log.add(folderModelResult);
            if (folderModelResult is! Success<FolderModel, io.FileSystemException>) return Failure(folderModelResult.asException, log);
            final result = await folderModelResult.wrapped.createAsComplement(newPath.location);
            log.add(result);
            return Danger.fromDanger(result, log);
        case Link():
            final linkModelResult = await LinkModel.fromPath(copyPath);
            log.add(linkModelResult);
            if (linkModelResult is! Success<LinkModel, io.FileSystemException>) return Failure(linkModelResult.asException, log);
            final result = await linkModelResult.wrapped.createAsComplement(newPath.location);
            log.add(result);
            return Danger.fromDanger(result, log);
        case _:
            final message = FileSystemModelExceptionA.LOGGER_RESULT_MESSAGE.join('\n');
            return Failure(io.FileSystemException(message, fileSystem.path), log);
        }

    }

    /// {@template FileSystemModel.move}
    /// [path] は移動対象を示し file, folder, link, ではない場合 失敗する.<br>
    /// [location] は移動先を示し 移動先に既存の file system が存在した場合失敗する.<br>
    /// {@endtemplate}
    static FutureDanger<Complete, io.FileSystemException> move(String path, String location) async {

        final log = Log(classLocation: FileSystemModel, functionLocation: 'move');

        final fileSystemModelResult = await FileSystemModel.fromPath(path);
        log.add(fileSystemModelResult);
        if (fileSystemModelResult is! Success<FileSystemModel, io.FileSystemException>) return Failure(fileSystemModelResult.asException, log);
        final fileSystemModel = fileSystemModelResult.wrapped;

        final fileSystemResult = FileSystem.fromLocationAndName(location, fileSystemModel.name);
        log.add(fileSystemResult);
        // 移動先path
        final fileSystem = fileSystemResult.wrapped;

        if (fileSystem is! NotFound) return Failure(
            io.FileSystemException(FileSystemModelExceptionR.LOGGER_RESULT_MESSAGE.join('\n'), fileSystem.path),
            log,
        );

        // 作成と削除 をトランザクション処理

        final createComplementResult = await fileSystemModel.createAsComplement(fileSystem.location);
        log.add(createComplementResult);
        if (createComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createComplementResult.asException, log);

        final deleteResult = await FileSystemModel.delete(path);
        log.add(deleteResult);
        if (deleteResult is! Success<Complete, io.FileSystemException>) {

            // 元の file system の削除に失敗したら 直前の作成した file system も削除する.
            final result = await FileSystemModel.delete(fileSystem.path);
            log.add(result);
            if (result is! Success<Complete, io.FileSystemException>) return Failure(result.asException, log.monitor({
                'message': 'move 処理の トランザクション処理に失敗.'
            }));
            return Failure(deleteResult.asException, log);

        }

        return Danger.fromDanger(createComplementResult, log);

    }

    static Danger<FileSystemModel, FileSystemModelExceptionST> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: FileSystemModel, functionLocation: 'fromJson');

        if (json.containsKey(FileModel.JSON_KEY_CLASS)) {
            final result = FileModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(FolderModel.JSON_KEY_CLASS)) {
            final result = FolderModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(EmptyLinkModel.JSON_KEY_CLASS)) {
            final result = EmptyLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(FileLinkModel.JSON_KEY_CLASS)) {
            final result = FileLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(FolderLinkModel.JSON_KEY_CLASS)) {
            final result = FolderLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid key set': JSON_KEY_CLASS_SET,
        }));

    }

    @override
    String get primaryKey => name;

    @override
    Map<String, Object> get properties {
        return {
            'name': name,
            'content': content,
        };
    }

    @override
    Result<FileSystemModel> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor);

    /// {@template FileSystemModel.exists}
    /// 実際に存在するものに応じた [FileSystem] を返す.<br>
    /// [FileSystemModelRoster]でも利用する.
    /// {@endtemplate}
    FutureSafety<FileSystem> exists(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'exists');

        final typeResult = FileSystem.fromLocationAndName(location, name);
        log.add(typeResult);

        return Safety(typeResult.wrapped, log);

    }

    /// {@template FileSystemModel.decode}
    /// すべての file system model で decode が行える必要がある.<br>
    /// でないと test で不便.<br>
    /// {@endtemplate}
    Danger<FileSystemModel, FormatException> decode([Encoding encoding = utf8]);

}
