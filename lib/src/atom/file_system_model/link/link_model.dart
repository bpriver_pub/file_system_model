// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

sealed class LinkModel
    extends
        FileSystemModel
{

    static const Set<String> JSON_KEY_CLASS_SET = {
        EmptyLinkModel.JSON_KEY_CLASS,
        FileLinkModel.JSON_KEY_CLASS,
        FolderLinkModel.JSON_KEY_CLASS,
    };

    @override
    final String name;

    @override
    final String content;

    LinkModel(this.name, this.content);

    /// 引数の link が file の link なら [FileLinkModel] を, folder の link なら [FolderLinkModel] を, link が無効なら [EmptyLinkModel] を返す.
    static FutureDanger<LinkModel, io.FileSystemException> fromPath(String path) async {

        final log = Log(classLocation: LinkModel, functionLocation: 'fromPath');

        final fileSystemResult = FileSystem.fromPath(path);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        return Danger.tryAndCatch(
            () {
                
                // 1.
                final link = io.Link(fileSystem.path);
                final content = link.targetSync();

                switch (fileSystem) {
                case EmptyLink():
                    return EmptyLinkModel(fileSystem.name, content);
                case FileLink():
                    return FileLinkModel(fileSystem.name, content);
                case FolderLink():
                    return FolderLinkModel(fileSystem.name, content);
                case _:
                    // 1. で FileSystemException が throw されないのなら fileSystem は必ず link であるため.
                    // これは io.FileSystemException ではないため catch されない.
                    Result.panic(FileSystemModelErrorC(), log);
                }

            },
            log,
        );

    }

    static Danger<LinkModel, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: LinkModel, functionLocation: 'fromJson');

        if (json.containsKey(EmptyLinkModel.JSON_KEY_CLASS)) {
            final result = EmptyLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(FileLinkModel.JSON_KEY_CLASS)) {
            final result = FileLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        if (json.containsKey(FolderLinkModel.JSON_KEY_CLASS)) {
            final result = FolderLinkModel.fromJson(json);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

        return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid key set': JSON_KEY_CLASS_SET,
        }));

    }

    @override
    Safety<FileSystemModel> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor) {

        final log = Log(classLocation: runtimeType, functionLocation: 'changeExhaustive');

        final editorResult = editor(this);

        return Safety(editorResult, log);

    }
    
    @override
    FutureDanger<Complete, io.FileSystemException> createAsComplement(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsComplement');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;
        
        return Danger.tryAndCatch(
            () {
                switch (fileSystem) {
                case NotFound():
                    final link = io.Link(fileSystem.path);
                    link.createSync(content, recursive: false);
                case _:;
                }
                return Complete();
            },
            log,
        );

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsOverwrite(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsOverwrite');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        final link = io.Link(fileSystem.path);

        switch (fileSystem) {
        case Link():
            return await Danger.tryAndCatchFuture(
                () async {
                    link.updateSync(content);
                    return Complete();
                },
                log,
            );
        case File():
        case Folder():
        case Pipe():
        case UnixDomainSocket():
            final deleteResult = await FileSystemModel.delete(fileSystem.path);
            log.add(deleteResult); 
            if (deleteResult is! Success<Complete, io.FileSystemException>) return Failure(deleteResult.asException, log);
            continue createCase;
        createCase:
        case NotFound():
            return await Danger.tryAndCatchFuture(
                () async {
                    link.createSync(content, recursive: false);
                    return Complete();
                },
                log,
            );
        }

    }

    FutureDanger<String, io.FileSystemException> target(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'target');

        final fileSystemResult = FileSystem.fromLocationAndName(location, name);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        final link = io.Link(fileSystem.path);
        
        return Danger.tryAndCatch(
            () => link.targetSync(),
            log,
        );

    }

    // decode は行わないので 常に 自身を返す.
    Success<LinkModel, FormatException> decode([Encoding encoding = utf8]) {

        final log = Log(classLocation: runtimeType, functionLocation: 'decode');

        return Success(this, log);

    }

}
