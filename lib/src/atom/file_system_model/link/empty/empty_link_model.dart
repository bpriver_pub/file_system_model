// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template EmptyLinkModel}
/// 何かしらの理由で link 先が存在しなくなった link.
/// {@endtemplate}
final class EmptyLinkModel
    extends
        LinkModel
    implements
        TreatedAsNoneModel
{

    static const String JSON_KEY_CLASS = 'EmptyLinkModel';
    static const String JSON_KEY_NAME = 'name';
    static const String JSON_KEY_CONTENT = 'content';

    EmptyLinkModel(super.name, [super.content = '']);

    static FutureDanger<EmptyLinkModel, io.FileSystemException> fromPath(String path) async {

        final log = Log(classLocation: EmptyLinkModel, functionLocation: 'fromPath');

        final fromPathResult = FileSystem.fromPath(path);
        log.add(fromPathResult);
        final pathStruct = fromPathResult.wrapped;

        return Danger.tryAndCatch(
            () {
                final link = io.Link(pathStruct.path);
                final content = link.targetSync();
                final result = EmptyLinkModel(pathStruct.name, content);
                return result;
            },
            log,
        );

    }

    static Danger<EmptyLinkModel, FileSystemModelExceptionS> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: EmptyLinkModel, functionLocation: 'fromJson');

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyNameResult = jsonKeyClassResult[JSON_KEY_NAME];
        if (jsonKeyNameResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_NAME key result': jsonKeyNameResult,
            '$JSON_KEY_NAME key result type': jsonKeyNameResult.runtimeType,
        }));

        final jsonKeyContentResult = jsonKeyClassResult[JSON_KEY_CONTENT];
        if (jsonKeyContentResult is! String) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_CONTENT key result': jsonKeyContentResult,
            '$JSON_KEY_CONTENT key result type': jsonKeyContentResult.runtimeType,
        }));

        final result = EmptyLinkModel(jsonKeyNameResult, jsonKeyContentResult);

        return Success(result, log);

    }

    EmptyLinkModel change({
        String? name,
        String? content,
    }) {
        return EmptyLinkModel(
            name ?? this.name,
            content ?? this.content,
        );
    }

}
