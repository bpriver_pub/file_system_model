// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

typedef FileModelRoster = FileSystemModelRoster<FileModel>;
typedef FolderModelRoster = FileSystemModelRoster<FolderModel>;
typedef LinkModelRoster = FileSystemModelRoster<LinkModel>;
typedef EmptyLinkModelRoster = FileSystemModelRoster<EmptyLinkModel>;
typedef FileLinkModelRoster = FileSystemModelRoster<FileLinkModel>;
typedef FolderLinkModelRoster = FileSystemModelRoster<FolderLinkModel>;
typedef TreatedAsFileModelRoster = FileSystemModelRoster<TreatedAsFileModel>;
typedef TreatedAsFolderModelRoster = FileSystemModelRoster<TreatedAsFolderModel>;
typedef TreatedAsNoneModelRoster = FileSystemModelRoster<TreatedAsNoneModel>;
