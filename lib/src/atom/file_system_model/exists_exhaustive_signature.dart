// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

/// {@template ExistsExhaustiveSignature}
/// 
/// folder 内の file, folder, link, も含めて存在を確認する. <br>
/// 
/// {@endtemplate}
abstract class ExistsExhaustiveSignature
{

    /// {@macro ExistsExhaustiveSignature}
    FutureSafety<bool> existsExhaustive(String location);

}