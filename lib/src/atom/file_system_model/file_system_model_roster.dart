// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/atom/file_system_model/file_system_model.dart';

class FileSystemModelRoster<
        VT extends FileSystemModel
    >
    extends
        Computation
    with
        RosterPattern<
            String
            ,VT
        >
    implements
        ChangeExhaustiveSignature
        ,CreateAsComplementSignature
        ,CreateAsOverwriteSignature
        ,ExistsExhaustiveSignature
{

    static const String JSON_KEY_CLASS = 'FileSystemModelRoster';
    static const String JSON_KEY_VALUES = 'values';

    // 【たぶん全集】Dartで配列(List)をソート(Sort)する方法 〔Flutter〕 - 退屈なエンジニアブログ: "https://atsu-developer.net/1103/"
    static Iterable<T> _sortByAlphabet<T extends FileSystemModel>(Iterable<T> values) {
        
        final names = List.generate(values.length, (index) => values.elementAt(index).name);
        
        names.sort((a, b) => a.compareTo(b));
        
        final sortedValues = List.generate(names.length, (index) {
            final name = names.elementAt(index);
            final value = values.firstWhere((element) => element.name == name);
            return value;
        });
        
        return sortedValues;

    }

    @override
    final Iterable<VT> values;

    const FileSystemModelRoster.empty()
    :
        values = const []
        ,super.const$()
    ;

    FileSystemModelRoster._(this.values);

    /// {@template FileSystemModelRoster.generativeConstructor}
    /// RosterPattern による == による等価は その list がもつ要素の順番が異なると false を返す.<br>
    /// なので、ここで順番を統一することで、順番が違うことにより == で false が返ることを防いでいる.<br>
    /// {@endtemplate}
    factory FileSystemModelRoster(Iterable<VT> values) {

        final log = Log(classLocation: FileSystemModelRoster, functionLocation: '');

        // VT の type 毎に処理を変える必要がある.
        // でなければ cast error が発生する.
        switch (VT) {
        case FileModel:
        case FolderModel:
        case FileLinkModel:
        case FolderLinkModel:
        case EmptyLinkModel:
        case TreatedAsFileModel:
        case TreatedAsFolderModel:
        case TreatedAsNoneModel:

            final sortResult = _sortByAlphabet(values);

            return FileSystemModelRoster._(sortResult);

        case FileSystemModel:
        case LinkModel:

            List<TreatedAsFolderModel> treatedAsFolder = [];
            List<TreatedAsFileModel> treatedAsFile = [];
            List<TreatedAsNoneModel> treatedAsNoneModel = [];

            for (final e in values) {

                switch (e) {
                    case TreatedAsFolderModel(): treatedAsFolder.add(e);
                    case TreatedAsFileModel(): treatedAsFile.add(e);
                    case TreatedAsNoneModel(): treatedAsNoneModel.add(e);
                }

            }

            final sortedFolder = _sortByAlphabet(treatedAsFolder);
            final sortedFile = _sortByAlphabet(treatedAsFile);
            final sortedNone = _sortByAlphabet(treatedAsNoneModel);

            final sortedFileSystemModel = <FileSystemModel>[
                ...sortedFolder,
                ...sortedFile,
                ...sortedNone,
            ];

            return FileSystemModelRoster._(sortedFileSystemModel as Iterable<VT>);
        
        case _:
            Result.panic(FileSystemModelErrorC(), log.monitor({
                'VT': VT,
            }));

        }

    }

    static Danger<FileSystemModelRoster, RosterPatternExceptionA> result<VT extends FileSystemModel>(Iterable<VT> values) => Danger.tryAndCatch(() => FileSystemModelRoster(values), Log(classLocation: FileSystemModelRoster, functionLocation: 'result'));

    /// {@template FileSystemModelRoster.fromYaml}
    /// template 部分の yaml を受け取る.
    /// {@endtemplate}
    static Danger<FileSystemModelRoster, BpriverYamlExceptionH> fromYamlMap(YamlMap yamlMap) {

        final log = Log(classLocation: FileSystemModelRoster, functionLocation: 'fromYaml');

        final List<FileSystemModel> list = [];

        // keys は MappedIterable<dynamic, dynamic> を返すため 型セーフ な書き方ができない.
        for (final i in yamlMap.keys) {

            final key = i as String;

            final Danger<Danger<FileSystemModel, BpriverYamlExceptionH>, BpriverYamlExceptionDH> getByKeyResult = BpriverYaml.getByKeyFromYamlMapThenAction<Danger<FileSystemModel, BpriverYamlExceptionH>>(
                yamlMap,
                key,
                // list の場合 exception. list の書き方は許容していない.
                // map が nest している場合 網羅処理を行う.
                thenMap: (value) {
                    final partContentResult = FileSystemModelRoster.fromYamlMap(value);
                    log.add(partContentResult);
                    if (partContentResult is! Success<FileSystemModelRoster, BpriverYamlExceptionH>) return Failure(partContentResult.asException, log);
                    final result = FolderModel(key, partContentResult.wrapped);
                    return Success(result, log);
                },
                // なぜか Scalar で wrap されていない 値が取得されることがある.
                // 下記の例のような場合 xxx が key で value として String が取得される.
                // ex:
                //      xxx: ''
                thenString: (value) {
                    final result = FileModel(key, value);
                    return Success(result, log);
                },
                thenBoolean: (value) {
                    final result = FileModel(key, value.toString());
                    return Success(result, log);
                },
                thenNumber: (value) {
                    final result = FileModel(key, value.toString());
                    return Success(result, log);
                },
                // 下記の例のような場合 xxx が key で value として null が取得される.
                // ex:
                //      xxx: null
                thenNull: () {
                    final result = FolderModel(key);
                    return Success(result, log);
                },
            );
            log.add(getByKeyResult);
            if (getByKeyResult is! Success<Danger<FileSystemModel, BpriverYamlExceptionH>, BpriverYamlExceptionDH>) {
                final exception = getByKeyResult.asException;
                switch (exception) {
                case BpriverYamlExceptionD(): Result.panic(FileSystemModelErrorB(), log.monitor({'exception': exception}));
                case BpriverYamlExceptionH(): return Failure(exception, log);
                }
            }

            final fileSystemModelResult = getByKeyResult.wrapped;
            if (fileSystemModelResult is! Success<FileSystemModel, BpriverYamlExceptionH>) return Failure(fileSystemModelResult.asException, log);

            list.add(fileSystemModelResult.wrapped);

        }

        final result = FileSystemModelRoster.result(list);
        log.add(result);

        // key により 一意であることが保証されているため RosterPatternExceptionA を throw out.
        return Danger.throwOut(result, FileSystemModelErrorB());

    }

    static Danger<FileSystemModelRoster, FileSystemModelExceptionST> fromJson(Map<String, dynamic> json) {

        final log = Log(classLocation: FileSystemModelRoster, functionLocation: 'fromJson');

        final List<FileSystemModel> list = [];

        final jsonKeyClassResult = json[JSON_KEY_CLASS];
        if (jsonKeyClassResult is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
            'invalid keys': json.keys,
            'valid class key': JSON_KEY_CLASS,
        }));

        final jsonKeyValuesResult = jsonKeyClassResult[JSON_KEY_VALUES];
        if (jsonKeyValuesResult is! Iterable) return Failure(FileSystemModelExceptionS(), log.monitor({
            '$JSON_KEY_VALUES key result': jsonKeyValuesResult,
            '$JSON_KEY_VALUES key result type': jsonKeyValuesResult.runtimeType,
        }));

        for (final value in jsonKeyValuesResult) {
            
            if (value is! Map<String, dynamic>) return Failure(FileSystemModelExceptionS(), log.monitor({
                'invalid values element': value,
                'invalid values element type': value.runtimeType,
            }));

            final result = FileSystemModel.fromJson(value);
            log.add(result);
            if (result is! Success<FileSystemModel, FileSystemModelExceptionST>) return Failure(result.asException, log);

            list.add(result.wrapped);

        }

        final result = FileSystemModelRoster.result(list);
        log.add(result);
        if (result is! Success<FileSystemModelRoster<FileSystemModel>, RosterPatternExceptionA>) return Failure(FileSystemModelException.fromRosterPatternExceptionA(result.asException), log);

        return Success(result.wrapped, log);

    }

    @override
    Danger<FileSystemModelRoster, RosterPatternExceptionA> changeExhaustive(FileSystemModel Function(FileSystemModel old) editor) {

        final log = Log(classLocation: runtimeType, functionLocation: 'changeExhaustive');

        final List<FileSystemModel> list = [];

        for (final e in values) {

            switch (e) {
            case FileModel():
                final eChangeExhaustiveResult = e.changeExhaustive(editor);
                log.add(eChangeExhaustiveResult);
                list.add(eChangeExhaustiveResult.wrapped);
            case LinkModel():
                final eChangeExhaustiveResult = e.changeExhaustive(editor);
                log.add(eChangeExhaustiveResult);
                list.add(eChangeExhaustiveResult.wrapped);
            case FolderModel():
                final eChangeExhaustiveResult = e.changeExhaustive(editor);
                log.add(eChangeExhaustiveResult);
                if (eChangeExhaustiveResult is! Success<FileSystemModel, RosterPatternExceptionA>) return Failure(eChangeExhaustiveResult.asException, log);
                list.add(eChangeExhaustiveResult.wrapped);
            }

        }

        final result = FileSystemModelRoster.result(list);
        log.add(result);

        return Danger.fromDanger(result, log);

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsComplement(String location) async {
        
        final log = Log(classLocation: runtimeType, functionLocation: 'createAsComplement');

        for (final e in values) {
            
            final createAsComplementResult = await e.createAsComplement(location);
            log.add(createAsComplementResult);

            if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createAsComplementResult.asException, log);

        }

        return Success(Complete(), log);

    }

    @override
    FutureDanger<Complete, io.FileSystemException> createAsOverwrite(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'createAsOverwrite');

        for (final e in values) {
            
            final createAsOverwriteResult = await e.createAsOverwrite(location);
            log.add(createAsOverwriteResult);

            if (createAsOverwriteResult is! Success<Complete, io.FileSystemException>) return Failure(createAsOverwriteResult.asException, log);

        }

        return Success(Complete(), log);

    }

    @override
    FutureSafety<bool> existsExhaustive(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'existsAsExhaustive');

        for (final e in values) {

            switch (e) {
            case FileModel():
                final existsResult = await e.exists(location);
                log.add(existsResult);
                switch (existsResult.wrapped) {
                    case File():
                        break;
                    case _:
                        return Safety(false, log);
                }
            case LinkModel():
                final existsResult = await e.exists(location);
                log.add(existsResult);
                switch (existsResult.wrapped) {
                    case Link():
                        break;
                    case _:
                        return Safety(false, log);
                }
            case FolderModel():
                final existsExhaustiveResult = await e.existsExhaustive(location);
                log.add(existsExhaustiveResult);
                if (!existsExhaustiveResult.wrapped) return Safety(false, log);
            }

        }

        return Safety(true, log);

    }

    // /// {@macro FolderModel.synchronize}
    FutureDanger<Complete, io.FileSystemException> synchronize(String location) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'synchronize');

        final fileSystemResult = FileSystem.fromPath(location);
        log.add(fileSystemResult);
        final fileSystem = fileSystemResult.wrapped;

        // location が 存在する folder だった場合のみ処理を続ける.
        if (fileSystem is! Folder) return Failure(
            io.FileSystemException(
                FileSystemModelExceptionO.LOGGER_RESULT_MESSAGE.join('\n'),
                fileSystem.path,
            ),
            log,
        );

        // location 配下を空にする.
        final Danger<Complete, io.FileSystemException> tryAndCatchResult = Danger.tryAndCatch(
            () {
                final folder = io.Directory(location);
                folder.deleteSync(recursive: true);
                folder.createSync(recursive: false);
                return Complete();
            },
            log,
        );
        if (tryAndCatchResult is! Success<Complete, io.FileSystemException>) return Failure(tryAndCatchResult.asException, log);

        for (final e in values) {
            
            final createAsComplementResult = await e.createAsComplement(location);
            log.add(createAsComplementResult);
            if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(createAsComplementResult.asException, log);

        }

        return Success(Complete(), log);

    }

    /// {@template FileSystemModelRoster.create}
    /// 
    /// {@endtemplate}
    FutureDanger<Complete, io.FileSystemException> create(String location, [GenerateMode generateMode = const ComplementMode()]) async {

        final log = Log(classLocation: runtimeType, functionLocation: 'create');

        switch (generateMode) {
        case ComplementMode():
            final result = await createAsComplement(location);
            log.add(result);
            return Danger.fromDanger(result, log);
        case OverwriteMode():
            final result = await createAsOverwrite(location);
            log.add(result);
            return Danger.fromDanger(result, log);
        case SynchronizeMode():
            final result = await synchronize(location);
            log.add(result);
            return Danger.fromDanger(result, log);
        }

    }

    /// {@template FileSystemModelRoster.decode}
    /// [BinaryContent] を [StringContent] へ変換する.
    /// {@endtemplate}
    Danger<FileSystemModelRoster, FormatException> decode([Encoding encoding = utf8]) {

        final log = Log(classLocation: runtimeType, functionLocation: 'encode');

        final List<FileSystemModel> list = [];

        for (final e in values) {

            switch (e) {
            case FileModel():
                final result = e.decode(encoding);
                log.add(result);
                if (result is! Success<FileModel, FormatException>) return Failure(result.asException, log);
                list.add(result.wrapped);
            case FolderModel():
                final result = e.decode(encoding);
                log.add(result);
                if (result is! Success<FolderModel, FormatException>) return Failure(result.asException, log);
                list.add(result.wrapped);
            case _:
                list.add(e);
            }

        }

        final result = FileSystemModelRoster.result(list);
        log.add(result);

        // 変更されるのは file model の content のみなので RosterPatternExceptionA を throw out.
        return Danger.throwOut(result, FileSystemModelErrorB(), log);

    }

}


