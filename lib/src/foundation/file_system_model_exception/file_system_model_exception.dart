// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/foundation/foundation_layer.dart';

/// {@template FileSystemModelException}
/// {@endtemplate}
/// * [FileSystemModelExceptionA]
/// {@macro FileSystemModelExceptionA}
/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionH]
/// {@macro FileSystemModelExceptionH}
/// * [FileSystemModelExceptionI]
/// {@macro FileSystemModelExceptionI}
/// * [FileSystemModelExceptionJ]
/// {@macro FileSystemModelExceptionJ}
/// * [FileSystemModelExceptionK]
/// {@macro FileSystemModelExceptionK}
/// * [FileSystemModelExceptionL]
/// {@macro FileSystemModelExceptionL}
/// * [FileSystemModelExceptionM]
/// {@macro FileSystemModelExceptionM}
/// * [FileSystemModelExceptionN]
/// {@macro FileSystemModelExceptionN}
/// * [FileSystemModelExceptionO]
/// {@macro FileSystemModelExceptionO}
/// * [FileSystemModelExceptionP]
/// {@macro FileSystemModelExceptionP}
/// * [FileSystemModelExceptionQ]
/// {@macro FileSystemModelExceptionQ}
/// * [FileSystemModelExceptionR]
/// {@macro FileSystemModelExceptionR}
/// * [FileSystemModelExceptionS]
/// {@macro FileSystemModelExceptionS}
/// * [FileSystemModelExceptionT]
/// {@macro FileSystemModelExceptionT}
/// * [FileSystemModelExceptionU]
/// {@macro FileSystemModelExceptionU}
/// * [FileSystemModelExceptionV]
/// {@macro FileSystemModelExceptionV}
/// * [FileSystemModelExceptionW]
/// {@macro FileSystemModelExceptionW}
/// * [FileSystemModelExceptionX]
/// {@macro FileSystemModelExceptionX}
/// * [FileSystemModelExceptionY]
/// {@macro FileSystemModelExceptionY}
/// * [FileSystemModelExceptionZ]
/// {@macro FileSystemModelExceptionZ}
sealed class FileSystemModelException

    with    
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const FileSystemModelException();

    static FileSystemModelExceptionD fromFileSystemException(io.FileSystemException exception) => FileSystemModelExceptionD();
    static FileSystemModelExceptionP fromFormatException(FormatException exception) => FileSystemModelExceptionP();
    static FileSystemModelExceptionT fromRosterPatternExceptionA(RosterPatternExceptionA exception) => FileSystemModelExceptionT();

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template FileSystemModelExceptionA}
/// 指定した path が file, folder, link, のいずれでもない.
/// {@endtemplate}
class FileSystemModelExceptionA
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionAD
        ,FileSystemModelExceptionADI
        ,FileSystemModelExceptionADNQ
        ,FileSystemModelExceptionADNR
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定した path が file, folder, link, のいずれでもない.',
        // 'path = $path',
        // 'type = $type',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionA}
    FileSystemModelExceptionA();

}

/// {@template FileSystemModelExceptionD}
/// copied from [io.FileSystemException].<br>
/// Exception thrown when a file operation fails.
/// {@endtemplate}
class FileSystemModelExceptionD
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionAD
        ,FileSystemModelExceptionADI
        ,FileSystemModelExceptionDH
        ,FileSystemModelExceptionDJ
        ,FileSystemModelExceptionDK
        ,FileSystemModelExceptionDL
        ,FileSystemModelExceptionDM
        ,FileSystemModelExceptionDN
        ,FileSystemModelExceptionDO
        ,FileSystemModelExceptionADNQ
        ,FileSystemModelExceptionADNR
        ,FileSystemModelExceptionDNO
{

    static const LOGGER_RESULT_MESSAGE = [
        'Exception thrown when a file operation fails.',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionD}
    FileSystemModelExceptionD();

}

/// {@template FileSystemModelExceptionH}
/// the path is not file.
/// {@endtemplate}
class FileSystemModelExceptionH
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDH
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not file.',
        // 'path = ${path}',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionH}
    FileSystemModelExceptionH();

}

/// {@template FileSystemModelExceptionI}
/// the path is not folder.
/// {@endtemplate}
class FileSystemModelExceptionI
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionADI
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not folder.',
        // 'path = ${path}',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionI}
    FileSystemModelExceptionI();

}

/// {@template FileSystemModelExceptionJ}
/// the path is not file link.
/// {@endtemplate}
class FileSystemModelExceptionJ
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDJ
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not file link.',
        // 'path = ${path}',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionJ}
    FileSystemModelExceptionJ();

}

/// {@template FileSystemModelExceptionK}
/// the path is not folder link.
/// {@endtemplate}
class FileSystemModelExceptionK
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDK
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not folder link.',
        // 'path = ${path}',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionK}
    FileSystemModelExceptionK();
     
}

/// {@template FileSystemModelExceptionL}
/// the path is not link.
/// {@endtemplate}
class FileSystemModelExceptionL
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDL
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not link.',
        // 'path = ${path}',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionL}
    FileSystemModelExceptionL();

}

/// {@template FileSystemModelExceptionM}
/// the path is not empty link.
/// {@endtemplate}
class FileSystemModelExceptionM
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDM
{

    static const LOGGER_RESULT_MESSAGE = [
        'the path is not empty link.',
        // 'path = ${path}',
        // // 'link target = ${link.targetSync()}' 
        // // targetSync() は link が実際には file や folder だった場合に throw exception.
        // // なのでやめた.
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionM}
    FileSystemModelExceptionM();

}

/// {@template FileSystemModelExceptionN}
/// file system entity の削除に失敗.<br>
/// 指定された path に何も存在しない などの要因が考えられます.<br>
/// {@endtemplate}
class FileSystemModelExceptionN
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDN
        ,FileSystemModelExceptionADNQ
        ,FileSystemModelExceptionADNR
        ,FileSystemModelExceptionDNO
{

    static const LOGGER_RESULT_MESSAGE = [
        'file system entity の削除に失敗.',
        '指定された path に何も存在しない などの要因が考えられます.'
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionN}
    FileSystemModelExceptionN();

}

/// {@template FileSystemModelExceptionO}
/// not exists location.
/// {@endtemplate}
final class FileSystemModelExceptionO
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDO
        ,FileSystemModelExceptionDNO
{

    static const LOGGER_RESULT_MESSAGE = [
        'not exists location.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionO}
    const FileSystemModelExceptionO();

}

/// {@template FileSystemModelExceptionP}
/// copied from [FormatException].<br>
/// Exception thrown when a string or some other data does not have an expected format and cannot be parsed or processed.<br>
/// {@endtemplate}
final class FileSystemModelExceptionP
    extends
        FileSystemModelException
{

    static const LOGGER_RESULT_MESSAGE = [
        'Exception thrown when a string or some other data does not have an expected format and cannot be parsed or processed.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionP}
    const FileSystemModelExceptionP();

}

/// {@template FileSystemModelExceptionQ}
/// copy paste 先に folder, pipe, unix domain socket, のいずれかが存在する.
/// {@endtemplate}
final class FileSystemModelExceptionQ
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionADNQ
{

    static const LOGGER_RESULT_MESSAGE = [
        'copy paste 先に folder, pipe, unix domain socket, のいずれかが存在する.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionQ}
    const FileSystemModelExceptionQ();

}

/// {@template FileSystemModelExceptionR}
/// file もしくは folder, link, の移動先に既に file sytem が存在します.<br>
/// {@endtemplate}
final class FileSystemModelExceptionR
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionADNR
{

    static const LOGGER_RESULT_MESSAGE = [
        'file もしくは folder, link, の移動先に既に file sytem が存在します',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionR}
    const FileSystemModelExceptionR();

}

/// {@template FileSystemModelExceptionS}
/// from json exception.
/// {@endtemplate}
final class FileSystemModelExceptionS
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionST
{

    static const LOGGER_RESULT_MESSAGE = [
        'from json exception.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionS}
    const FileSystemModelExceptionS();

}

/// {@template FileSystemModelExceptionT}
/// exists duplicate primary key.<br>
/// copied from [RosterPatternExceptionA].<br>
/// {@endtemplate}
final class FileSystemModelExceptionT
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionST
{

    static const LOGGER_RESULT_MESSAGE = RosterPatternExceptionA.LOGGER_RESULT_MESSAGE;

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelExceptionT}
    const FileSystemModelExceptionT();

}

/// * [FileSystemModelExceptionA]
/// {@macro FileSystemModelExceptionA}
/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
sealed class FileSystemModelExceptionAD
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionADI
        ,FileSystemModelExceptionADNQ
        ,FileSystemModelExceptionADNR
{}

/// * [FileSystemModelExceptionA]
/// {@macro FileSystemModelExceptionA}
/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionI]
/// {@macro FileSystemModelExceptionI}
sealed class FileSystemModelExceptionADI
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionH]
/// {@macro FileSystemModelExceptionH}
sealed class FileSystemModelExceptionDH
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionJ]
/// {@macro FileSystemModelExceptionJ}
sealed class FileSystemModelExceptionDJ
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionK]
/// {@macro FileSystemModelExceptionK}
sealed class FileSystemModelExceptionDK
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionL]
/// {@macro FileSystemModelExceptionL}
sealed class FileSystemModelExceptionDL
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionM]
/// {@macro FileSystemModelExceptionM}
sealed class FileSystemModelExceptionDM
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionN]
/// {@macro FileSystemModelExceptionN}
sealed class FileSystemModelExceptionDN
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionADNQ
        ,FileSystemModelExceptionADNR
        ,FileSystemModelExceptionDNO
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionO]
/// {@macro FileSystemModelExceptionO}
sealed class FileSystemModelExceptionDO
    extends
        FileSystemModelException
    implements
        FileSystemModelExceptionDNO
{}

/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionN]
/// {@macro FileSystemModelExceptionN}
/// * [FileSystemModelExceptionO]
/// {@macro FileSystemModelExceptionO}
sealed class FileSystemModelExceptionDNO
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionA]
/// {@macro FileSystemModelExceptionA}
/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionN]
/// {@macro FileSystemModelExceptionN}
/// * [FileSystemModelExceptionQ]
/// {@macro FileSystemModelExceptionQ}
sealed class FileSystemModelExceptionADNQ
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionA]
/// {@macro FileSystemModelExceptionA}
/// * [FileSystemModelExceptionD]
/// {@macro FileSystemModelExceptionD}
/// * [FileSystemModelExceptionN]
/// {@macro FileSystemModelExceptionN}
/// * [FileSystemModelExceptionR]
/// {@macro FileSystemModelExceptionR}
sealed class FileSystemModelExceptionADNR
    extends
        FileSystemModelException
{}

/// * [FileSystemModelExceptionS]
/// {@macro FileSystemModelExceptionS}
/// * [FileSystemModelExceptionT]
/// {@macro FileSystemModelExceptionT}
sealed class FileSystemModelExceptionST
    extends
        FileSystemModelException
{}
