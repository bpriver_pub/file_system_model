// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/foundation/foundation_layer.dart';

/// {@template GenerateMode}
/// 
/// {@endtemplate}
sealed class GenerateMode extends Thin with EnumPattern {
    const GenerateMode();
    static Danger<GenerateMode, EnumPatternExceptionA> fromValue(String value) {
        final log = Log(classLocation: GenerateMode, functionLocation: 'fromValue');
        switch (value) {
            case ComplementMode.VALUE: return Success(ComplementMode(), log);
            case OverwriteMode.VALUE: return Success(OverwriteMode(), log);
            case SynchronizeMode.VALUE: return Success(SynchronizeMode(), log);
            case _: return Failure(EnumPatternExceptionA(), log.monitor({ 'invalid value': value, 'valid values': GenerateMode.VALUE_SET }));
        }
    }
    static Risk<RT, EnumPatternErrorA> fromType<RT extends GenerateMode>() {
        final log = Log(classLocation: GenerateMode, functionLocation: 'fromType');
        switch (RT) {
            case ComplementMode: return Risk(ComplementMode() as RT, log);
            case OverwriteMode: return Risk(OverwriteMode() as RT, log);
            case SynchronizeMode: return Risk(SynchronizeMode() as RT, log);
            case _: Result.panic(EnumPatternErrorA(), log.monitor({ 'invalid type': RT, 'valid types': FINAL_TYPE_SET }));
        }
    }
    static const Set<Type> FINAL_TYPE_SET = {
        ComplementMode,
        OverwriteMode,
        SynchronizeMode,
    };
    static const Set<GenerateMode> INSTANCE_SET = {
        ComplementMode(),
        OverwriteMode(),
        SynchronizeMode(),
    };
    static const Set<String> VALUE_SET = {
        ComplementMode.VALUE,
        OverwriteMode.VALUE,
        SynchronizeMode.VALUE,
    };
}

    final class ComplementMode extends GenerateMode { static const VALUE = 'complement'; const ComplementMode(); @override String get value => VALUE; }
    final class OverwriteMode extends GenerateMode { static const VALUE = 'overwrite'; const OverwriteMode(); @override String get value => VALUE; }
    final class SynchronizeMode extends GenerateMode { static const VALUE = 'synchronize'; const SynchronizeMode(); @override String get value => VALUE; }
