// Copyright (C) 2023, the file_system_model project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:file_system_model/src/foundation/foundation_layer.dart';

/// {@template FileSystemModelError}
/// {@endtemplate}
sealed class FileSystemModelError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template FileSystemModelErrorB}
/// 発生するはずのない exception が発生.
/// {@endtemplate}
class FileSystemModelErrorB
    extends
        FileSystemModelError
{

    static const LOGGER_RESULT_MESSAGE = [
        '発生するはずのない exception が発生.',
        // 'occurred exception = ${E}',
        // ...exception.loggerResultMessage,
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelErrorB}
    FileSystemModelErrorB();

}

/// {@template FileSystemModelErrorC}
/// 到達するはずのない process への到達.
/// {@endtemplate}
class FileSystemModelErrorC
    extends
        FileSystemModelError
{

    static const LOGGER_RESULT_MESSAGE = [
        '到達するはずのない process への到達.',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro FileSystemModelErrorC}
    FileSystemModelErrorC();

}
