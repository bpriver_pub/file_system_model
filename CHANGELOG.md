## 0.6.1
- Update bpriver_origin library
    - 0.6.1

## 0.6.0
- Update bpriver_origin library
    - 0.6.0
- Delete bpriver_yaml library
    - bpriver_origin library に統合

## 0.5.5
- Update bpriver yaml
    - 0.5.4

## 0.5.4
- Update bpriver yaml
    - 0.5.3

## 0.5.3
- Delete StringContent.toJson
- Edit BinaryContent.fromJson
- Update bpriver origin
    - 0.5.2
- Update bpriver yaml
    - 0.5.2

## 0.5.2
- Update bpriver origin
    - 0.5.1
- Update bpriver yaml
    - 0.5.1

## 0.5.1
- Edit BinaryContent.fromJson
    - 空の file だった時の処理.
    - 空の場合 List<dynamic> が返る.

## 0.5.0
- Update bpriver origin
    - 0.5.0
- Update bpriver yaml
    - 0.5.0

## 0.4.4
- Update bpriver origin
    - 0.4.2
- Update bpriver yaml
    - 0.4.2

## 0.4.3
- 細かい修正

## 0.4.2
- Edit fromJson
    - 自身の class name を key とした json を必要なように変更.
- Add JSON_KEY

## 0.4.1
- Add fromJson
- Add StringContent.toJson
    - 既存のままでは json に変換できないので 実装を override する必要がある.
- Edit FileModel generative constructor
    - encoding を指定できるように.

## 0.4.0
- Update bpriver origin
    - 0.4.0
- Update bpriver yaml
    - 0.4.0

## 0.3.4
- Update bpriver origin
    - 0.3.4
- Update bpriver yaml
    - 0.3.4

## 0.3.3
- Update bpriver origin
    - 0.3.3
- Update bpriver yaml
    - 0.3.3

## 0.3.2
- Update bpriver origin
    - 0.3.2
- Update bpriver yaml
    - 0.3.1

## 0.3.1
- Edit decode method

## 0.3.0
- Update dart sdk
    - 3.6.0

## 0.0.1

## 0.0.2
add FolderModelCreateSpecific

## 0.0.3
edit FolderModelCreateSpecific